﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Task02_PageLifeCycle.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>PageLifeCycle</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button runat="server" OnClick="OnClick" Text="Button For PostBack"/>
        <asp:Table runat="server" ID="mainTable" GridLines="Both" Font-Names="Times New Roman"
        Font-Size="12pt" 
        CellPadding="10" 
        CellSpacing="0" ></asp:Table>
    </div>
    </form>
</body>
</html>