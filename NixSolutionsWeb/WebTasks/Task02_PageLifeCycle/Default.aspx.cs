﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Task02_PageLifeCycle
{
    public partial class Default : Page
    {
        // Не позволяет обработать тело метода который был привязан к событию вызвавшее постбэк 
        //protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl, string eventArgument)
        //{
        //    Response.Write("RaisePostBackEvent " + DateTime.Now.ToLocalTime() + "<br />");
        //}

        protected override void LoadViewState(object savedState)
        {
            Response.Write("LoadViewState " + DateTime.Now.ToLocalTime() + "<br />");
        }

        public override void Validate()
        {
            Response.Write("Validate " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected override object LoadPageStateFromPersistenceMedium()
        {
            Response.Write("LoadPageStateFromPersistenceMedium " + DateTime.Now.ToLocalTime() + "<br />");
            return base.LoadPageStateFromPersistenceMedium();
        }

        protected override void OnSaveStateComplete(EventArgs e)
        {
            Response.Write("OnSaveStateComplete " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            Response.Write("OnPreRenderComplete " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected override void OnPreLoad(EventArgs e)
        {
            Response.Write("OnPreLoad " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected override void OnLoad(EventArgs e)
        {
            Response.Write("OnLoad " + DateTime.Now.ToLocalTime() + "<br />");;
        }

        protected override void OnPreInit(EventArgs e)
        {
            Response.Write("OnPreInit " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            Response.Write("OnLoadComplete " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected override void OnInitComplete(EventArgs e)
        {
            Response.Write("OnInitComplete " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected override void OnPreRender(EventArgs e)
        {
            Response.Write("OnPreRender " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected override void OnInit(EventArgs e)
        {
            Response.Write("OnInit " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write("Page_Load " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            Response.Write("Page_PreInit " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Response.Write("Page_Init " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected void Page_InitComplete(object sender, EventArgs e)
        {
            Response.Write("Page_InitComplete " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            Response.Write("Page_PreLoad " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            Response.Write("Page_LoadComplete " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Response.Write("Page_PreRender " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected void Page_SaveStateComplete(object sender, EventArgs e)
        {
            Response.Write("Page_SaveStateComplete " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected void Page_Render(object sender, EventArgs e)
        {
            Response.Write("Page_Render " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected void Page_PreRenderComplete(object sender, EventArgs e)
        {
            Response.Write("Page_PreRenderComplete " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected void Page_UnLoad(object sender, EventArgs e)
        {
            //На этапе выгрузки страница и элементы управления в ней отрисовываются,
            //поэтому внесение изменений в поток ответа не допускается.
            //При попытке вызова метода Response.Write или подобных ему возникает исключение.
            // Соответственно вызвать метод OnUnLoad тоже нельзя, так как он генерирует событие Page_UnLoad
            //Response.Write("Page_UnLoad " + DateTime.Now.ToLocalTime() + "<br />");
        }

        protected void OnClick(object sender, EventArgs e)
        {
            // Request.Form - является коллекцией типа словаря содержащая ключи и значения
            // Ключ - название контрола который находится на окне
            // Значение - обычно берется свойства Текст для элемента управления
            // В данном случае на форме из видимых элементов есть только Button, который так же содержится в этой коллекции
            // Но помимо него существует еще 3 невидимых конечному пользователю контрола, в которых хранится служебная информация
            // Например: Key - _VIEWSTATE, Value - состояние объектов на текущей странице в сериализованном виде
            // Например: Key - _EVENTVALIDATION, Value - сериализованная информация о том какой элемент управления вызвал постбэк на сервер
            var coll = Request.Form.AllKeys;
            foreach (var items in coll)
            {
                var row = new TableRow();
                var mainCell = new TableCell();
                mainCell.Controls.Add(new LiteralControl(items));
                row.Cells.Add(mainCell);
                foreach (var value in Request.Form.GetValues(items))
                {
                    var cell = new TableCell();
                    cell.Controls.Add(new LiteralControl(value));
                    row.Cells.Add(cell);
                }
                mainTable.Rows.Add(row);
            }
        }
    }
}