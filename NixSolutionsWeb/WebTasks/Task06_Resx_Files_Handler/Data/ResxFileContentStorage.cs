﻿/*
    @author Adzhoev M.
*/
using System.Resources;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.ComponentModel.Design;
using System.Xml.Linq;
using System.Linq;

namespace Task06_Resx_Files_Handler
{
    // Describes repository of resx file content
    public class ResxFileContentStorage
    {
        // path to resx file
        private static string ResxFileName
        {
            get
            {
                return (string)HttpContext.Current.Application["currentFile" + HttpContext.Current.Request.UserHostAddress + HttpContext.Current.Request.Browser.Browser];
            }
        }
        // returns list of resources from selected resx file
        public List<ResxFileContent> GetAll()
        {
            List<ResxFileContent> content = new List<ResxFileContent>();
            if (ResxFileName == null)
            {
                return null;
            }
            using (ResXResourceReader reader = new ResXResourceReader(HttpContext.Current.Server.MapPath(@"~/Resources/" + ResxFileName)))
            {
                reader.UseResXDataNodes = true;
                IDictionaryEnumerator enumerator = reader.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    ResXDataNode node = (ResXDataNode)enumerator.Value;
                    content.Add(new ResxFileContent()
                    {
                        ID = node.Name,
                        Name = node.Name,
                        Value = node.GetValue((ITypeResolutionService)null).ToString(),
                        Comment = node.Comment
                    });
                }
            }
            return content;
        }
        // inserts resource to resx file
        public void InsertResource(string Name, string Value, string Comment)
        {
            XDocument document = XDocument.Load(HttpContext.Current.Server.MapPath(@"~/Resources/" + ResxFileName));
            var xElement = document.Element("root");
            xElement?.Add(
                new XElement("data", new XAttribute("name", Name),
                    new XElement("value", Value),
                    new XElement("comment", Comment)                  
                    ));
            document.Save(HttpContext.Current.Server.MapPath(@"~/Resources/" + ResxFileName));
        }
        // updates resource state in resx file
        public void UpdateResource(string ID, string Name, string value, string comment)
        {
            if (comment == null)
            {
                comment = "";
            }
            if (value == null)
            {
                value = "";
            }
            XDocument document = XDocument.Load(HttpContext.Current.Server.MapPath(@"~/Resources/" + ResxFileName));
            var target = document.Root.Elements("data").Where(l => (string)l.Attribute("name") == ID).Single();
            target.Element("value").Value = value;
            target.Attribute("name").Value = Name;
            if (target.Element("comment") == null)
            {
                target.Add(new XElement("comment"));
            }
            target.Element("comment").Value = comment;
            document.Save(HttpContext.Current.Server.MapPath(@"~/Resources/" + ResxFileName));
        }
        // deletes resource from resx file(do not work actually)
        public void DeleteResource(string ID)
        {
            XDocument doc = XDocument.Load(HttpContext.Current.Server.MapPath(@"~/Resources/" + ResxFileName));
            doc.Root.Elements("data")
                .Where(l => l.Attribute("name").Value.Equals(ID))
                .Remove();
            doc.Save(HttpContext.Current.Server.MapPath(@"~/Resources/" + ResxFileName));
        }
    }
}