﻿/*
    @author Adzhoev M.
*/

namespace Task06_Resx_Files_Handler
{
    // describes typical resx file item with attribute data
    public class ResxFileContent
    {
        // unique id of resource(equals name)
        public string ID { get; set; }
        // name of resource
        public string Name { get; set; }
        // resource value
        public string Value { get; set; }
        // comment(can be empty)
        public string Comment { get; set; }
    }
}