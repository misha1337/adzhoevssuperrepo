﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Task06_Resx_Files_Handler.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server" method="post">
        <div>
            <p>Выбери файл ресурсов для работы с его содержимым: </p>
            <asp:DropDownList AutoPostBack="true" Width="320px" ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"></asp:DropDownList>
            <asp:Label runat="server" ID="errorlbl" />
        </div>
        <br />
        <asp:GridView ID="GridView1"
            ShowFooter="true"
            runat="server"
            AutoGenerateColumns="False"
            DataSourceID="ObjectDataSource1"
            AllowPaging="True"
            PageSize="20">
            <Columns>
                <asp:TemplateField HeaderText="Name" HeaderStyle-ForeColor="Yellow" ItemStyle-Width="605px">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="nameLabel" Text='<%# Bind("Name") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ValidationGroup="editVal" runat="server" ID="nameBox" Text='<%# Bind("Name") %>' />
                        <asp:RequiredFieldValidator runat="server" ID="rfvNameEdit" ControlToValidate="nameBox"
                            ForeColor="Red" ValidationGroup="editVal" Text="Key cannot be empty" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox runat="server" ID="nameAddBox" ValidationGroup="addVal" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvName" ControlToValidate="nameAddBox"
                            ForeColor="Red" ValidationGroup="addVal" Text="Key cannot be empty" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Value" HeaderStyle-ForeColor="Yellow" ItemStyle-Width="605px">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="valueLabel" Text='<%# Bind("Value") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="valueBox" Text='<%# Bind("Value") %>' />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox runat="server" ID="valueAddBox" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Comment" HeaderStyle-ForeColor="Yellow" ItemStyle-Width="605px">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="commentLabel" Text='<%# Bind("Comment") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="commentBox" Text='<%# Bind("Comment") %>' />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox runat="server" ID="commentAddBox" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="200px">
                    <EditItemTemplate>
                        <asp:LinkButton ID="ButtonUpdate" runat="server" CommandName="Update" Text="Update" ValidationGroup="editVal" />
                        <asp:LinkButton ID="ButtonCancel" runat="server" CommandName="Cancel" Text="Cancel" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="ButtonEdit" runat="server" CommandName="Edit" Text="Edit" />
                        <asp:LinkButton ID="ButtonDelete" runat="server" CommandName="Delete" Text="Delete" />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Button OnClick="ButtonAdd_Click" ID="ButtonAdd" runat="server" CommandName="Insert" Text="Add New Record" ValidationGroup="addVal" />
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="Blue" BorderColor="Black" />
        </asp:GridView>
        <asp:ObjectDataSource ID="ObjectDataSource1"
            OnInserted="Binder"
            OnInserting="ObjectDataSource1_Inserting"
            OnDeleted="Binder"
            runat="server"
            SelectMethod="GetAll"
            TypeName="Task06_Resx_Files_Handler.ResxFileContentStorage"
            DeleteMethod="DeleteResource" UpdateMethod="UpdateResource" InsertMethod="InsertResource">
            <InsertParameters>
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="Value" Type="String" />
                <asp:Parameter Name="Comment" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="ID" Type="String" />
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="value" Type="String" />
                <asp:Parameter Name="comment" Type="String" />
            </UpdateParameters>
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="String" />
            </DeleteParameters>
        </asp:ObjectDataSource>
    </form>
</body>
</html>
