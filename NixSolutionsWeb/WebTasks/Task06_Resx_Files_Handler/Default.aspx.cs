﻿/*
    @author Adzhoev M.
*/

using System;
using System.IO;
using System.Web.Configuration;
using System.Web.UI.WebControls;

namespace Task06_Resx_Files_Handler
{
    public partial class Default : System.Web.UI.Page
    {
        // full pathes to resx files in selected directory
        string[] files;

        protected override void CreateChildControls()
        {
            GridView1.DataKeyNames = new string[] { "ID" };
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!TryLoadResourceFiles())
                    {
                        Controls.Clear();
                        errorlbl.Text = "There is no resource files in current directory or directory is not exists";
                        errorlbl.ForeColor = System.Drawing.Color.Red;
                        Controls.Add(errorlbl);
                        return;
                    }
                    foreach (var file in files)
                    {
                        if (Path.GetExtension(file) == ".resx")
                        {
                            DropDownList1.Items.Add(new ListItem(file.Substring(file.LastIndexOf(Path.DirectorySeparatorChar) + 1)));
                        }
                    }
                    Application["currentFile" + Request.UserHostAddress + Request.Browser.Browser] = DropDownList1.Items[0].Text;
                }
                catch (Exception exc)
                {
                    errorlbl.Text = exc.Message;
                    errorlbl.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
        // tries to load resx files from selected in appconfig directory
        private bool TryLoadResourceFiles()
        {
            int counter = 0;
            string folderpath = WebConfigurationManager.AppSettings["pathToResxFiles"];
            try
            {
                files = Directory.GetFiles(Server.MapPath(folderpath));
            }
            catch
            {
                return false;
            }
            foreach (var file in files)
            {
                if (Path.GetExtension(file) == ".resx")
                    counter++;
            }
            return counter != 0;
        }
        // happends when user changes selection in drop down list
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Application["currentFile" + Request.UserHostAddress + Request.Browser.Browser] = DropDownList1.SelectedItem.Value;
            GridView1.DataBind();
        }
        // happends when user clicks on resource add button(calls object data source insert method from binded repository)
        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            ObjectDataSource1.Insert();
        }
        // happends before inserting process
        protected void ObjectDataSource1_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            string name = ((TextBox)GridView1.FooterRow.FindControl("nameAddBox")).Text;
            string value = ((TextBox)GridView1.FooterRow.FindControl("valueAddBox")).Text;
            string comment = ((TextBox)GridView1.FooterRow.FindControl("commentAddBox")).Text;
            e.InputParameters["Name"] = name;
            e.InputParameters["Value"] = value;
            e.InputParameters["Comment"] = comment;
        }
        // happends when information on grid view needs in updating
        protected void Binder(object sender, ObjectDataSourceStatusEventArgs e)
        {
            GridView1.DataBind();
        }
    }
}