﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="Task09_ProductsList_AJAX.Upload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server" method="post">
        <asp:FileUpload runat="server" ID="FileUpload" />
        <asp:Button runat="server" Text="Залить файл на сервер" OnClick="Upload_Click" />
        <br />
        <asp:Label runat="server" ID="errorLabel" />
    </form>
</body>
</html>

