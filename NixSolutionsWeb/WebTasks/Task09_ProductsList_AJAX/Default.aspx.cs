﻿using System;
using System.Web.UI.WebControls;

namespace Task09_ProductsList_AJAX
{
    public partial class Default : System.Web.UI.Page
    {
        public static Button MoveForwardButton;
        public static Button MoveBackButton;
        protected void Page_Load(object sender, EventArgs e)
        {
            MoveForwardButton = moveforwardBtn;
            MoveBackButton = movebackBtn;
            movebackBtn.Click += ProductsView.MoveBack;
            moveforwardBtn.Click += ProductsView.MoveForward;
        }
    }
}