﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoadProducts.aspx.cs" Inherits="Task09_ProductsList_AJAX.LoadProducts" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:LinkButton runat="server" Text="Upload product image on server to select it than" OnClick="Unnamed_Click2"></asp:LinkButton>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" ShowFooter="true" DataSourceID="ObjectDataSource1">
                <Columns>
                    <asp:TemplateField HeaderText="Name" ItemStyle-Width="605px">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="nameLabel" Text='<%# Bind("Name") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ValidationGroup="editVal" runat="server" ID="nameBox" Text='<%# Bind("Name") %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox runat="server" ID="nameAddBox" ValidationGroup="addVal" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Image name on server" ItemStyle-Width="605px">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="valueLabel" Text='<%# Bind("ImageUrl") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList runat="server" ID="editDDL" OnLoad="Unnamed_Load"></asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList runat="server" OnLoad="Unnamed_Load" ID="imageDDL"></asp:DropDownList>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Price" ItemStyle-Width="605px">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="commentLabel" Text='<%# Bind("Price") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox runat="server" ID="commentBox" Text='<%# Bind("Price") %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox runat="server" ID="priceAddBox" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="200px">
                        <EditItemTemplate>
                            <asp:LinkButton ID="ButtonUpdate" runat="server" CommandName="Update" Text="Update" ValidationGroup="editVal" />
                            <asp:LinkButton ID="ButtonCancel" runat="server" CommandName="Cancel" Text="Cancel" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="ButtonEdit" runat="server" CommandName="Edit" Text="Edit" />
                            <asp:LinkButton ID="ButtonDelete" runat="server" CommandName="Delete" Text="Delete" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Button OnClick="ButtonAdd_Click" ID="ButtonAdd" runat="server" CommandName="Insert" Text="Add product" ValidationGroup="addVal" />
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <asp:ObjectDataSource OnUpdating="ObjectDataSource1_Updating" UpdateMethod="Update" OnUpdated="ObjectDataSource1_Deleted" OnInserted="ObjectDataSource1_Deleted" OnDeleted="ObjectDataSource1_Deleted" DeleteMethod="Delete" OnInserting="ObjectDataSource1_Inserting" InsertMethod="Insert" ID="ObjectDataSource1" runat="server" 
            SelectMethod="GetAll" TypeName="Task09_ProductsList_AJAX.Data.Storage">
            <UpdateParameters>
                <asp:Parameter Name="ID" Type="String" />
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="ImageUrl" Type="String" />
                <asp:Parameter Name="Price" Type="String" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="ImageUrl" Type="String" />
                <asp:Parameter Name="Price" Type="String" />
            </InsertParameters>
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="String" />
            </DeleteParameters>
        </asp:ObjectDataSource>
    </form>
</body>
</html>
