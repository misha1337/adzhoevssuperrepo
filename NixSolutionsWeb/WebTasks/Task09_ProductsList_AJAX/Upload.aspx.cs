﻿/**
    @author Adzhoev Mikhail
*/

using System;
using System.IO;
using System.Drawing;

namespace Task09_ProductsList_AJAX
{
    // class with logic of page to upload images to server from clients computer
    public partial class Upload : System.Web.UI.Page
    {
        // uploads images to server
        protected void Upload_Click(object sender, EventArgs e)
        {
            if (!FileUpload.HasFile) return;
            var filename = Path.GetFileName(FileUpload.FileName);
            FileUpload.SaveAs(Server.MapPath("~/Images/") + filename);
            try
            {
                var myBitmap = new Bitmap(Server.MapPath("~/Images/") + filename);
                errorLabel.Text = "File successfully uploaded!";
                errorLabel.ForeColor = System.Drawing.Color.LightGreen;
            }
            catch
            {
                errorLabel.Text = "Upload status: cant upload file to server. The following error occured: image format is incorrent";
                errorLabel.ForeColor = System.Drawing.Color.Red;
                File.Delete(Server.MapPath("~/Images/") + filename);
            }
        }
    }
}