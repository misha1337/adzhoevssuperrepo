﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Task09_ProductsList_AJAX.Default" %>

<%@ Register Src="~/ProductsView.ascx" TagPrefix="uc1" TagName="ProductsView" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:ScriptManager runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server" GridLines="Both" CellSpacing="20">
                    <asp:TableRow>
                        <asp:TableCell><asp:Button runat="server" Id="movebackBtn" Text="Move back"/></asp:TableCell>
                        <asp:TableCell><uc1:ProductsView runat="server" id="ProductsView" ProductToViewAmount="3" /></asp:TableCell>
                        <asp:TableCell><asp:Button runat="server" Id="moveforwardBtn" Text="Move forward"/></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>      
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
