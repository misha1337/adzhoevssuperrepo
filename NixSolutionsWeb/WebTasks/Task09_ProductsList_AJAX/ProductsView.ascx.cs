﻿/**
    @author Adzhoev Mikhail
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Task09_ProductsList_AJAX
{
    // products view control
    public partial class ProductsView : System.Web.UI.UserControl
    {
        // current index of product to view
        private int CurrentIndex
        {
            get
            {
                if (Session["CurrentIndex"] == null)
                {
                    return 0;
                }
                return (int)Session["CurrentIndex"];
            }

            set
            {
                Session["CurrentIndex"] = value;
            }
        }
        // amount of products to view by user
        public int ProductToViewAmount { get; set; }
        // products list loaded from xml file
        private List<Data.Product> _products = new Data.Storage().GetAll().ToList();
        
        protected override void CreateChildControls()
        {
            if (CurrentIndex == 0)
                Default.MoveBackButton.Enabled = false;
            else
                Default.MoveBackButton.Enabled = true;
            if (CurrentIndex + ProductToViewAmount >= _products.Count)
                Default.MoveForwardButton.Enabled = false;
            else
                Default.MoveForwardButton.Enabled = true;
        }
        // moves forward products list, by button clicking
        public void MoveForward(object sender, EventArgs e)
        {
            HtmlTable table = new HtmlTable();
            table.CellSpacing = 20;
            HtmlTableRow row = new HtmlTableRow();
            table.Rows.Add(row);
            CurrentIndex += ProductToViewAmount;
            for (var i = CurrentIndex; i < CurrentIndex + ProductToViewAmount; i++)
            {
                if (i == _products.Count) break;
                SetInformation(row, i);
            }
            Controls.Add(table);
        }
        // moves back products list, by button clicking
        public void MoveBack(object sender, EventArgs e)
        {
            HtmlTable table = new HtmlTable();
            table.CellSpacing = 20;
            HtmlTableRow row = new HtmlTableRow();
            table.Rows.Add(row);
            CurrentIndex -= ProductToViewAmount;
            for (var i = CurrentIndex; i < CurrentIndex + ProductToViewAmount; i++)
            {
                if (i == _products.Count) break;
                SetInformation(row, i);
            }
            Controls.Add(table);
        }
        //sets product information in table cell(name, price, photo)
        private void SetInformation(HtmlTableRow row, int i)
        {
            HtmlTableCell cell = new HtmlTableCell();
            cell.Controls.Add(new Label { Text = string.Concat(_products[i].Name, "<br />") });
            cell.Controls.Add(new Image { ImageUrl = Server.MapPath(@"~/Images/" + _products[i].ImageUrl), Width = 200, Height = 100 });
            cell.Controls.Add(new Label { Text = string.Concat("<br />", _products[i].Price) });
            row.Cells.Add(cell);
        }
        // loads first products from file to table
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HtmlTable table = new HtmlTable();
                Session["CurrentIndex"] = 0;
                table.CellSpacing = 20;
                HtmlTableRow row = new HtmlTableRow();
                table.Rows.Add(row);
                for (var i = 0; i < ProductToViewAmount; i++)
                {
                    if (i == _products.Count) break;
                    SetInformation(row, i);
                }
                Controls.Add(table);
            }
        }
    }
}