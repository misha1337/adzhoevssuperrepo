﻿/**
    @author: Adzhoev Mikhail
*/

namespace Task09_ProductsList_AJAX.Data
{
    public class Product
    {
        // product unique id
        public string Id { get; set; }
        // product name
        public string Name { get; set; }
        // url to image on server
        public string ImageUrl { get; set; }
        // product price
        public string Price { get; set; }
    }
}