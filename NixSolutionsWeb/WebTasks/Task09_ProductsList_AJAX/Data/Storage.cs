﻿/**
    @author: Adzhoev Mikhail
*/

using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Task09_ProductsList_AJAX.Data
{
    // xml file storage operations
    public class Storage
    {
        private static string _fileName = HttpContext.Current.Server.MapPath("Products.xml");
        // Xml document as storage link
        private readonly XDocument _document = XDocument.Load(_fileName);

        public IEnumerable<Data.Product> GetAll()
        {
            return from product in _document.Descendants("Product")
                   select new Data.Product
                   {
                       Id = product.Element("Name").Value,
                       Name = product.Element("Name").Value,
                       ImageUrl = product.Element("ImageUrl").Value,
                       Price = product.Element("Price").Value
                   };
        }

        public void Insert(string Name, string ImageUrl, string Price)
        {
            XDocument document = XDocument.Load(_fileName);
            var xElement = document.Element("Products");
            xElement?.Add(
                new XElement("Product",
                    new XElement("Name", Name),
                    new XElement("ImageUrl", ImageUrl),
                    new XElement("Price", Price)
                    ));
            document.Save(HttpContext.Current.Server.MapPath("Products.xml"));
        }

        public void Delete(string ID)
        {
            if (ID == null)
            {
                ID = "";
            }
            XDocument doc = XDocument.Load(_fileName);
            doc.Root.Elements("Product")
                .Where(l => l.Element("Name").Value.Equals(ID))
                .Remove();
            doc.Save(HttpContext.Current.Server.MapPath("Products.xml"));
        }

        public void Update(string ID, string Name, string ImageUrl, string Price)
        {
            if (Price == null)
            {
                Price = "";
            }
            if(Name == null)
            {
                Name = "";
            }
            XDocument document = XDocument.Load(_fileName);
            var target = document.Root.Elements("Product").Where(l => l.Element("Name").Value == ID).Single();
            target.Element("ImageUrl").Value = ImageUrl;
            target.Element("Price").Value = Price;
            target.Element("Name").Value = Name;
            document.Save(HttpContext.Current.Server.MapPath("Products.xml"));
        }
    }
}
