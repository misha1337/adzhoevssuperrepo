﻿/**
    @author: Adzhoev Mikhail
*/

using System;
using System.IO;
using System.Web.UI.WebControls;

namespace Task09_ProductsList_AJAX
{
    // load, edit and remove products from xml file storage
    public partial class LoadProducts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.DataKeyNames = new string[] { "ID" };
        }
        // add product button click
        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            ObjectDataSource1.Insert();
        }
        // redirects user to upload picture page
        protected void Unnamed_Click2(object sender, EventArgs e)
        {
            Response.Redirect("Upload.aspx");
        }
        // loads images list to all drop downs on page
        protected void Unnamed_Load(object sender, EventArgs e)
        {
            var ddl = sender as DropDownList;
            foreach (var item in Directory.GetFiles(Server.MapPath(@"~/Images/")))
            {
                ddl.Items.Add(item.Substring(item.LastIndexOf(Path.DirectorySeparatorChar) + 1));
            }
        }
        // takes information from controls in footer row on insert in storage
        protected void ObjectDataSource1_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            string name = ((TextBox)GridView1.FooterRow.FindControl("nameAddBox")).Text;
            string imageurl = ((DropDownList)GridView1.FooterRow.FindControl("imageDDL")).Text;
            string price = ((TextBox)GridView1.FooterRow.FindControl("priceAddBox")).Text;
            e.InputParameters["Name"] = name;
            e.InputParameters["ImageUrl"] = imageurl;
            e.InputParameters["Price"] = price;
        }
        // updates gridview control after deleted, inserted, updated
        protected void ObjectDataSource1_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            GridView1.DataBind();
        }
        // takes information from gridview control to update
        protected void ObjectDataSource1_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            var imageurl = ((DropDownList)GridView1.Rows[GridView1.EditIndex].FindControl("editDDL")).Text;
            e.InputParameters["ImageUrl"] = imageurl;
        }
    }
}