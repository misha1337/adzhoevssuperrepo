﻿/**
@author Adzhoev Mikhail
*/

using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Task10_Membership
{
    // default page with admins panel
    public partial class Default : System.Web.UI.Page
    {
        // checkbox to allow admin make updates without password changing
        private CheckBox dontChangePasswordCheckBox = new CheckBox { Text = "Don't change password."
            , ToolTip= "Check if you do not want the system checks the password and repeat fields", AutoPostBack = true
        };
        // information table cell, with personal selected user info
        private TableCell informationCell;
        // user name textbox(readonly)
        private TextBox nameBox = new TextBox { ReadOnly = true };
        // email textbox(autovalidate for correct email)
        private TextBox emailBox = new TextBox { TextMode = TextBoxMode.Email };
        // password textbox, text hidden by dots
        private TextBox passwordBox = new TextBox { TextMode = TextBoxMode.Password };
        // password repeat textbox, text hidden by dots
        private TextBox repeatPasswordBox = new TextBox { TextMode = TextBoxMode.Password };
        // radio button list with users logins
        private RadioButtonList usersList = new RadioButtonList { AutoPostBack = true };
        // password label
        private Label _passwordCaption = new Label { Text = "Password: ", Width = 120 };
        // password repeat label
        private Label _passwordRepeatCaption = new Label { Text = "Repeat password: ", Width = 120 };

        protected override void CreateChildControls()
        {
            ScriptManager manager = new ScriptManager();
            dontChangePasswordCheckBox.CheckedChanged += DontChangePasswordCheckBox_CheckedChanged;
            UpdatePanel panel = new UpdatePanel();
            Table contentTable = new Table { CellSpacing = 70 };
            contentTable.Style.Value = "margin: auto; padding: 10px; border: 1px black solid;";
            TableRow contentRow = new TableRow();
            contentTable.Rows.Add(contentRow);
            TableCell listCell = new TableCell();
            informationCell = new TableCell();
            contentRow.Cells.Add(listCell);
            contentRow.Cells.Add(informationCell);
            usersList.SelectedIndexChanged += UsersList_SelectedIndexChanged;
            foreach (MembershipUser user in Membership.GetAllUsers())
            {
                usersList.Items.Add(user.UserName);
            }

            Button deleteButton = new Button { Text = "Delete selected user" };
            deleteButton.BackColor = System.Drawing.Color.Aquamarine;
            deleteButton.Click += DeleteButton_Click;

            Button acceptUpdate = new Button { Text = "Accept updating" };
            acceptUpdate.Click += AcceptUpdate_Click;
            acceptUpdate.BackColor = System.Drawing.Color.Aquamarine;

            listCell.Controls.Add(usersList);
            listCell.Controls.Add(deleteButton);
            informationCell.Controls.Add(CreateUpdateInformationControl());
            informationCell.Controls.Add(acceptUpdate);
            informationCell.Controls.Add(dontChangePasswordCheckBox);
            informationCell.Visible = false;

            panel.ContentTemplateContainer.Controls.Add(contentTable);

            form1.Controls.Add(new Literal { Text = "Nice to see you, " });
            form1.Controls.Add(new LoginName());
            form1.Controls.Add(new LoginStatus());
            form1.Controls.Add(manager);
            form1.Controls.Add(panel);
        }
        // changes password and password repeat control visible
        private void DontChangePasswordCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            passwordBox.Visible = repeatPasswordBox.Visible = _passwordCaption.Visible 
                = _passwordRepeatCaption.Visible = !dontChangePasswordCheckBox.Checked;
        }

        // accepting information updating
        private void AcceptUpdate_Click(object sender, EventArgs e)
        {
            Label errorLabel = new Label();
            errorLabel.ForeColor = System.Drawing.Color.Red;
            informationCell.Controls.Add(errorLabel);
            try
            {
                MembershipUser user = Membership.GetUser(nameBox.Text);
                if (!dontChangePasswordCheckBox.Checked)
                {
                    if (repeatPasswordBox.Text != passwordBox.Text)
                    {
                        errorLabel.Text = "<br />Incorrect password repeat";
                        return;
                    }
                    user.ChangePassword(user.ResetPassword(), passwordBox.Text);
                }
                user.Email = emailBox.Text;
                Membership.UpdateUser(user);
                errorLabel.ForeColor = System.Drawing.Color.Green;
                errorLabel.Text = "<br />User info was successfully changed";
                dontChangePasswordCheckBox.Checked = false;
                passwordBox.Visible = repeatPasswordBox.Visible = _passwordCaption.Visible
                = _passwordRepeatCaption.Visible = !dontChangePasswordCheckBox.Checked;
            }
            catch (Exception exc)
            {
                errorLabel.ForeColor = System.Drawing.Color.Red;
                errorLabel.Text = "<br />" + exc.Message;
            }
        }
        // delete selected user from database button click
        private void DeleteButton_Click(object sender, EventArgs e)
        {
            Membership.DeleteUser(usersList.SelectedItem.Text);
            usersList.Items.Clear();
            foreach (MembershipUser user in Membership.GetAllUsers())
            {
                usersList.Items.Add(user.UserName);
            }
            informationCell.Visible = false;
        }
        // selected user from radiobuttonlist index changed event
        private void UsersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            passwordBox.Text = "";
            repeatPasswordBox.Text = "";
            MembershipUser user = Membership.GetUser(usersList.SelectedItem.Text);
            nameBox.Text = user.UserName;
            emailBox.Text = user.Email;
            informationCell.Visible = true;
        }
        // creates control with textboxes and captions to allow administrator update information about users
        private Control CreateUpdateInformationControl()
        {
            Control viewUserControl = new Control();
            viewUserControl.Controls.Add(CreateInputWithCaptionPanel(new Label { Text = "Name: ", Width = 120 }, nameBox));
            viewUserControl.Controls.Add(CreateInputWithCaptionPanel(new Label { Text = "Email: ", Width = 120 }, emailBox));
            viewUserControl.Controls.Add(CreateInputWithCaptionPanel(_passwordCaption, passwordBox));
            viewUserControl.Controls.Add(CreateInputWithCaptionPanel(_passwordRepeatCaption, repeatPasswordBox));
            viewUserControl.Controls.Add(new Literal { Text = "<br />" });
            return viewUserControl;
        }
        // creates panel with label and text box
        private Panel CreateInputWithCaptionPanel(Label caption, TextBox infoBox)
        {
            Panel itemPanel = new Panel();
            itemPanel.Controls.Add(caption);
            itemPanel.Controls.Add(infoBox);
            return itemPanel;
        }
    }
}