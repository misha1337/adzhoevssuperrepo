﻿section = 1;

function setNextSection() {
	switch(section) {
		case 1: {
		if(!validateAuth()) {
			return;
		}
			setPage(1, 2);
			break;
		}
		case 2: {
		if(!validatePersonalInformation()) {
			return;
		}
			setPage(2, 3);
			break;
		}
		case 3: {
		if(!validateAddress()) {
			return;
		}
			setPage(3, 4);
			break;
		}
		case 4: {
		if(!validateSubscribe()) {
			return;	
		}
			setPage(4, 5);
			break;
		}
	}
	section++;
	if(section == 5) {
		getResults();
	}

}

function clearLabel() {
	$("#birthdayDate").html("");
}

function setPrevSection() {
	setPage(section, section-1);
	section--;
}

function subscribe() {
	var div = $("#subs_list");
	if(div.css("display") == "block") {
		div.hide();
		var inputElements = $('*[name=subscribe_list]');
		for(var i=0; i < inputElements.length; i++) {
      			inputElements[i].checked = false;
      		}
		return;
	}
	div.show();
}

function getBirthday() {
	var today = new Date();
	var text = $("#birthday").val();
	var temp = text.split("/");
	var birthDate = new Date(parseInt(temp[2], 10), parseInt(temp[1], 10) - 1, parseInt(temp[0], 10));
	if(Date.now() < birthDate.getTime())
	{
		$("#birthdayDate").html("Вы родились в будущем?");
		return;
	}	
	var ageDifMs = Date.now() - birthDate.getTime();
   	var ageDate = new Date(ageDifMs);
    	var age =  Math.abs(ageDate.getUTCFullYear() - 1970);
	if(!isNaN(age)) {
		$("#birthdayDate").html(age);
	}
}

function formAccept() {
	if(validateAuth() && validatePersonalInformation() && validateAddress() && validateSubscribe()) {
		alert('Регистрация прошла успешно');
	}
}

function setPage(pageNumber, anotherPageNumber) {
	$("#div"+ pageNumber).hide();
	$("#div"+ anotherPageNumber).show();
}

function getResults() {
	$("#loginResult").val($("#userLogin").val());
	$("#fioResult").val($("#fio").val());
	$("#birthdayResult").val($("#birthday").val());
	$("#addressResult").val($("#address").val());
	$("#cityResult").val($("#city").val());
	$("#regionResult").val($("#region option:selected").text());
	$("#indexResult").val($("#index").val());
	$("#emailResult").val($("#email").val());
	var checkedValue = ""; 
		var inputElements = $('*[name=subscribe_list]');
		for(var i=0; i < inputElements.length; i++) {
      		if(inputElements[i].checked) {
           		checkedValue += inputElements[i].value + " ";
      		}
		}
	$("#subscriptionsResult").val(checkedValue);
	$("#submitButton").css("display", "block");
}

function validateAuth() {
	var login = $("#userLogin").val();
	var password = $("#userPassword").val();
	var repeat = $("#repeatUserPassword").val();
	var loginTemplate = /^[A-Z]\w{3,}[0-9]$/;
	var passwordTemplate = /(?=.{8,})((?=.*\d)(?=.*[a-z])(?=.*[A-Z])|(?=.*\d)(?=.*[a-zA-Z])(?=.*[\W_])|(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_])).*/;
	var resultString = "";
	if(!loginTemplate.test(login)) {
		resultString += "Логин пользователя должен содержать минимум одну букву в верхнем регистре,\n должен заканчиваться на\n цифру, длина должна быть не меньше 5 символов,\nсостоять из английских букв и цифр.\n";
	}
	if(!passwordTemplate.test(password)) {
		resultString += "В пароле должны присутствовать символы трех категорий из числа следующих четырех:\n 1. прописные буквы английского алфавита от A до Z;\n 2. строчные буквы английского алфавита от a до z;\n 3. десятичные цифры (от 0 до 9);\n 4. неалфавитные символы (например, !, $, #, %)\n";
	}
	if(password != repeat || repeat == "") {
		resultString += "Повторный пароль не совпадает с указанным выше\n";
	}
	if(resultString != "") {
		alert(resultString);
		return false;
	}
	return true;
}

function validatePersonalInformation() {
	var fullNameTemplate = /^[А-Я][а-яА-Я]+ [А-Я][а-яА-Я]+ [А-Я][а-яА-Я]+$/;
	var birthdayTemplate = /^\d{2}\/\d{2}\/\d{4}$/;
	var birthday = $("#birthday");
	var fullName = $("#fio");
	var resultString = "";
	if(!fullNameTemplate.test(fullName.val())) {
		resultString += "Поле ФИО может содержать только символы и состоять из 3 слов, разделенных пробелом\n Начальный строчный символ запрещен\n";
	}
	if(!birthdayTemplate.test(birthday.val())) {
		resultString += "Неверный формат даты рождения(дд/мм/гггг)\n";
	}	
	else if(isNaN($("#birthdayDate").html()))
	{
		resultString += "Дата рождения не может быть больше нынешней даты";
	}
	else if(parseInt($("#birthdayDate").html()) < 12) {
		resultString += "Регистрация доступна c 12 лет\n";
	}
	if(resultString != "") {
		alert(resultString);
		return false;
	}
	return true;
}

function validateAddress() {
	var indexTemplate = /^\d{5}$/;
	if(!indexTemplate.test($("#index").val())) {
			alert("Индекс должен содержать 5 цифр!");
			return false;
	}
	return true;
}

function validateSubscribe() {
	var emailTemplate = /[0-9a-z_]+@[0-9a-z_]+\.[a-z]{2,5}/i;
	var resultString = "";
	var email = $("#email");
	if(!emailTemplate.test(email.val())) {
		resultString += "Неверный формат электронной почты\n";
	}
	if($("#subscribeCheckbox").prop("checked")) {
		var checkedValue = ""; 
		var inputElements = $('*[name=subscribe_list]');
		for(var i=0; i < inputElements.length; i++) {
      		if(inputElements[i].checked) {
           		checkedValue += inputElements[i].value;
      		}
		}
		if(checkedValue == "") {
			resultString += "Выберите хотя бы одну подписку!";
		}
	}
	if(resultString != "") {
		alert(resultString);
		return false;
	}
	return true;
}