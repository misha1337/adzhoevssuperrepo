﻿using System;
using System.IO;
using System.Linq;
using System.Web.Configuration;

namespace Task03_Application_Statistics
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            var path = WebConfigurationManager.AppSettings["pages"];
            ApplicationWrapper.PagesPathesList = File.ReadAllLines(Server.MapPath(path)).ToList();
            Application["applicationStartTime"] = DateTime.Now.Date;
            Application["allTimeRequestsCounter"] = 0;
            Application["oneDayRequestsCounter"] = 0;
            Application["uniqueUsersRequestsCounter"] = 0;
            Application["visitorsRequestsCounter"] = 0;
            Application["testPage1"] = 0;
            Application["testPage2"] = 0;
            Application["testPage3"] = 0;
            Application["statsPage"] = 0;
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Application.Lock();
            if ((DateTime)Application["applicationStartTime"] == DateTime.Now.Date)
            {
                ApplicationWrapper.VisitorsRequests++;
            }
            else
            {
                ApplicationWrapper.VisitorsRequests = 1;
            }
            if (ApplicationWrapper.ValidateVisitor(new ApplicationWrapper
                .UniqVisitor(Request.Browser.Browser, Request.UserHostAddress)) == null)
            {
                ApplicationWrapper.UniqueUsersRequests++;
            }
            Application.UnLock();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Application.Lock();
            if ((DateTime)Application["applicationStartTime"] == DateTime.Now.Date)
            {
                ApplicationWrapper.OneDayRequests++;
            }
            else
            {
                ApplicationWrapper.OneDayRequests = 1;
                Application["applicationStartTime"] = DateTime.Now.Date;
            }
            ApplicationWrapper.AllTimeRequests++;
            Application.UnLock();
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}