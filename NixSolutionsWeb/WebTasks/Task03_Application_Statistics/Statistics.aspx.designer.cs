﻿//------------------------------------------------------------------------------
// <автоматически создаваемое>
//     Этот код создан программой.
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода. 
// </автоматически создаваемое>
//------------------------------------------------------------------------------

namespace Task03_Application_Statistics {
    
    
    public partial class Statistics {
        
        /// <summary>
        /// form1 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// AllTimeStats элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label AllTimeStats;
        
        /// <summary>
        /// OneDayStats элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label OneDayStats;
        
        /// <summary>
        /// UniqUsersStats элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label UniqUsersStats;
        
        /// <summary>
        /// VisitorsStats элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label VisitorsStats;
        
        /// <summary>
        /// Page1Stats элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Page1Stats;
        
        /// <summary>
        /// Page2Stats элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Page2Stats;
        
        /// <summary>
        /// Page3Stats элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Page3Stats;
        
        /// <summary>
        /// test1 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton test1;
        
        /// <summary>
        /// test2 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton test2;
        
        /// <summary>
        /// test3 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton test3;
    }
}
