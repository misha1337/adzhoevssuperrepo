﻿using System;

namespace Task03_Application_Statistics.Pages
{
    public partial class TestPage3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Application.Lock();
            Application["testPage3"] = (int)Application["testPage3"] + 1;
            Response.Write(string.Concat("Количество запросов к данной странице: ", Application["testPage3"]));
            Application.UnLock();
        }
    }
}