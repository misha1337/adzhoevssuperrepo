﻿using System;

namespace Task03_Application_Statistics.Pages
{
    public partial class TestPage1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Application.Lock();
            Application["testPage1"] = (int)Application["testPage1"] + 1;
            Response.Write(string.Concat("Количество запросов к данной странице: ", Application["testPage1"]));
            Application.UnLock();
        }
    }
}