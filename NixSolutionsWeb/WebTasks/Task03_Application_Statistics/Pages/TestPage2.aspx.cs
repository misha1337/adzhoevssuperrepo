﻿using System;

namespace Task03_Application_Statistics.Pages
{
    public partial class TestPage2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Application.Lock();
            Application["testPage2"] = (int)Application["testPage2"] + 1;
            Response.Write(string.Concat("Количество запросов к данной странице: ", Application["testPage2"]));
            Application.UnLock();
        }
    }
}