﻿using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task03_Application_Statistics
{
    public static class ApplicationWrapper
    {
        // uniqal visitor information
        public class UniqVisitor
        {
            // browser name of a user
            public string BrowserName { get; set; }
            // ip address of a user
            public string IpAddress { get; set; }

            public UniqVisitor(string browserName, string ipAddress)
            {
                BrowserName = browserName;
                IpAddress = ipAddress;
            }
        }
        // list with pathes to pages
        public static List<string> PagesPathesList = new List<string>();
        // list of visitors
        private static readonly List<UniqVisitor> VisitorsList = new List<UniqVisitor>();
        // checks of the visitor is unique(by browser and ip address)
        public static object ValidateVisitor(UniqVisitor visitor)
        {
            var result = VisitorsList.FirstOrDefault(vis => (vis.BrowserName == visitor.BrowserName)
            && vis.IpAddress == visitor.IpAddress);
            if (result != null) return visitor;
            VisitorsList.Add(visitor);
            return null;
        }
        // all time web application requests
        public static int AllTimeRequests
        {
            get { return (int)HttpContext.Current.Application["allTimeRequestsCounter"]; }
            set
            {
                HttpContext.Current.Application["allTimeRequestsCounter"] = value;
            }
        }
        // one day web application requests
        public static int OneDayRequests
        {
            get { return (int)HttpContext.Current.Application["oneDayRequestsCounter"]; }
            set
            {
                HttpContext.Current.Application["oneDayRequestsCounter"] = value;
            }
        }
        // unique users requests
        public static int UniqueUsersRequests
        {
            get { return (int)HttpContext.Current.Application["uniqueUsersRequestsCounter"]; }
            set
            {
                HttpContext.Current.Application["uniqueUsersRequestsCounter"] = value;
            }
        }
        // one day visitors requests
        public static int VisitorsRequests
        {
            get { return (int)HttpContext.Current.Application["visitorsRequestsCounter"]; }
            set
            {
                HttpContext.Current.Application["visitorsRequestsCounter"] = value;
            }
        }
    }
}