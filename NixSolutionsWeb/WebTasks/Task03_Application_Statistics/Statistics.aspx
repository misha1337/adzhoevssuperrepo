﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Statistics.aspx.cs" Inherits="Task03_Application_Statistics.Statistics" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Statistic page.</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server" ID="AllTimeStats"></asp:Label>
            <asp:Label runat="server" ID="OneDayStats"></asp:Label>
            <asp:Label runat="server" ID="UniqUsersStats"></asp:Label>
            <asp:Label runat="server" ID="VisitorsStats"></asp:Label>
            <asp:Label runat="server" ID="Page1Stats"></asp:Label>
            <asp:Label runat="server" ID="Page2Stats"></asp:Label>
            <asp:Label runat="server" ID="Page3Stats"></asp:Label>
            <div>
                <asp:LinkButton runat="server" ID="test1">TestPage1 </asp:LinkButton>
                <asp:LinkButton runat="server" ID="test2">TestPage2 </asp:LinkButton>
                <asp:LinkButton runat="server" ID="test3">TestPage3</asp:LinkButton>
            </div>
        </div>
    </form>
</body>
</html>
