﻿using System;

namespace Task03_Application_Statistics
{
    public partial class Statistics : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Application.Lock();
            Application["statsPage"] = (int)Application["statsPage"] + 1;
            Application.UnLock();
            test1.PostBackUrl = ApplicationWrapper.PagesPathesList[0];
            test2.PostBackUrl = ApplicationWrapper.PagesPathesList[1];
            test3.PostBackUrl = ApplicationWrapper.PagesPathesList[2];
            AllTimeStats.Text = string.Concat("Количество запросов к приложению за все время: "
                , ApplicationWrapper.AllTimeRequests, "</br>");
            OneDayStats.Text = string.Concat("Количество запросов к приложению за сутки(от начала работы сервера): ",
                ApplicationWrapper.OneDayRequests, "</br>");
            UniqUsersStats.Text = string.Concat("Количество уникальных пользователей за все время: "
                , ApplicationWrapper.UniqueUsersRequests, "</br>");
            VisitorsStats.Text = string.Concat("Количество посетителей за день: ",
                ApplicationWrapper.VisitorsRequests, "</br>");
            Page1Stats.Text = string.Concat("Количество запросов к TestPage1: "
                , Application["testPage1"], "</br>");
            Page2Stats.Text = string.Concat("Количество запросов к TestPage2: "
                , Application["testPage2"], "</br>");
            Page3Stats.Text = string.Concat("Количество запросов к TestPage3: "
                , Application["testPage3"], "</br>");
            Response.Write(string.Concat("Количество запросов к данной странице: ", Application["statsPage"]));
        }
    }
}