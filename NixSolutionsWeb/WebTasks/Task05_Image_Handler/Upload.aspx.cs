﻿using System;
using System.IO;
using System.Web;
using System.Linq;
using System.Security.Cryptography;
using Task05_Image_Handler.Data;
using System.Drawing;

namespace Task05_Image_Handler
{
    // class with logic of page to upload images to server from clients computer
    public partial class Upload : System.Web.UI.Page
    {
        // handles event when page have been loaded
        protected void Page_Load(object sender, EventArgs e)
        {
            FillImageInfoList();
            errorLabel.Text = "";
        }
        // fills list with images info(imageName, iD)
        private static void FillImageInfoList()
        {
            ImagesCollection.Items.Clear();
            var names = Directory.EnumerateFiles(HttpContext.Current.Server.MapPath(@"~\Images\")).ToList();
            var ids = names.Select(item => EntryModule.GetMd5Hash(MD5.Create(), item)).ToList();
            for (var i = 0; i < names.Count; i++)
            {
                ImagesCollection.Items.Add(new ImageInfo(names[i], ids[i]));
            }
        }

        protected void Upload_Click(object sender, EventArgs e)
        {
            if (!FileUpload.HasFile) return;
            var filename = Path.GetFileName(FileUpload.FileName);
            try
            {              
                if (ImagesCollection.Items.FirstOrDefault(image => image.Imagename.Substring(image.Imagename
                    .LastIndexOf(Path.DirectorySeparatorChar) + 1) == filename) != null)
                {
                    errorLabel.Text = $"Upload status: image with name {filename} is already exists. Please rename your image.";
                    errorLabel.ForeColor = System.Drawing.Color.LightPink;
                    return;
                }
                FileUpload.SaveAs(Server.MapPath("~/Images/") + filename);
                var myBitmap = new Bitmap(Server.MapPath("~/Images/") + filename);
                string id = EntryModule.GetMd5Hash(MD5.Create(), filename);
                ImagesCollection.Items.Add(new ImageInfo(filename, id));
                errorLabel.Text = $"Upload status: File uploaded! Image ID: {id} ";
                errorLabel.ForeColor = System.Drawing.Color.LightGreen;
            }
            catch (ArgumentException)
            {
                errorLabel.Text = "Upload status: cant upload file to server. The following error occured: image format is incorrent";
                errorLabel.ForeColor = System.Drawing.Color.Red;
                File.Delete(Server.MapPath("~/Images/") + filename);
            }
            catch (Exception exc)
            {
                errorLabel.Text = "Upload status: cant upload file to server. The following error occured: " + exc.Message;
                errorLabel.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
}