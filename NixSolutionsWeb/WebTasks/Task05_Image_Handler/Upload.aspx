﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="Task05_Image_Handler.Upload" MasterPageFile="~/ImagehandlerMaster.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="browserTitle" ID="title">
    <title>Загрузка изображения</title>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1" ID="Content1">
    <div>
        <asp:FileUpload runat="server" ID="FileUpload" />
        <asp:Button runat="server" Text="Залить файл на сервер" OnClick="Upload_Click" />
        <br />
        <asp:Label runat="server" ID="errorLabel" />
    </div>
</asp:Content>
