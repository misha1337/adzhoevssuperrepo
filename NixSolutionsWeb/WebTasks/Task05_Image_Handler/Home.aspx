﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Task05_Image_Handler.Home" MasterPageFile="~/ImagehandlerMaster.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="browserTitle" ID="title">
    <title>Домашняя страница</title>
</asp:Content>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="content">
        <asp:LinkButton runat="server" Text="Залить изображение на сервер" OnClick="UploadButton_Click" />
        <asp:DropDownList runat="server" ID="images" /> <br /><br />
        <asp:Button runat="server" ID="submit" Text="Просмотреть изображение с указанным текстом" OnClick="SubmitButton_Click" />
    </div>
</asp:Content>

