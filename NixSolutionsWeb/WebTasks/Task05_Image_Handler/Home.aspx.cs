﻿using System;
using System.IO;

namespace Task05_Image_Handler
{
    // page when user can take changed image which have been selected in dropdownlist
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                foreach (var img in Directory.EnumerateFiles(Server.MapPath(@"~\Images")))
                {
                    images.Items.Add(img.Substring(img.LastIndexOf(Path.DirectorySeparatorChar) + 1));
                }
            }
        }
        // handles event when upload button have been clicked(redirects user to Upload.aspx page)
        protected void UploadButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("Upload.aspx");
        }
        // handles event when submit button have been clicked
        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            Application["selectedImage"] = images.SelectedItem.Text;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('Images/" + images.SelectedItem.Text + "?UserKey=b9e52202515e507b646867ae703c68f5','_newtab');", true);      
        }
    }
}