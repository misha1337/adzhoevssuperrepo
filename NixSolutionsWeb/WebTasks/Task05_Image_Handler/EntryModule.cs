﻿using System;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace Task05_Image_Handler
{
    // HttpModule to control access to each page on site
    public class EntryModule : IHttpModule
    {
        #region IHttpModule Members
        public void Dispose()
        {

        }
        // binds methods to common application event
        public void Init(HttpApplication context)
        {
            context.BeginRequest += Context_BeginRequest;
        }
        // gets md5 hash code
        public static string GetMd5Hash(MD5 md5Hash, string input)
        {
            var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            var sBuilder = new StringBuilder();
            foreach (var t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }
            return sBuilder.ToString();
        }
        // handles event when request to site page have been already started
        private static void Context_BeginRequest(object sender, EventArgs e)
        {
            // MD5 key: b9e52202515e507b646867ae703c68f5
            var application = (HttpApplication)sender;
            var context = application.Context;
            var request = context.Request;
            var response = context.Response;
            string key;
            using (var md5Hash = MD5.Create())
            {
                key = GetMd5Hash(md5Hash, "I_Am_Real_Admin");
            }
            if (!string.IsNullOrEmpty(request.Params["userkey"]) && request.Params["userkey"] == key) return;
            response.Clear();
            response.Write("You are not authorized.");
            response.End();
        }
        #endregion

    }
}
