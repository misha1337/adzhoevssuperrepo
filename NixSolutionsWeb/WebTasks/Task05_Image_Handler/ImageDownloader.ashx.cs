﻿using System.Drawing;
using System.Web;

namespace Task05_Image_Handler
{
    // httphandler which helps in showing image process
    public class ImageDownloader : IHttpHandler
    {
        // works when requesting process is working
        public void ProcessRequest(HttpContext context)
        {
            var response = context.Response;
            var toPrintText = System.Web.Configuration.WebConfigurationManager.AppSettings["ToPrintText"];
            var filename = context.Application["selectedImage"].ToString();
            var myBitmap = new Bitmap(context.Server.MapPath(@"~\Images\" + filename));
            var g = Graphics.FromImage(myBitmap);
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            g.DrawString(toPrintText, new Font("Tahoma", 24), Brushes.Chocolate, new Point(myBitmap.Width - 100, myBitmap.Height - 50));
            myBitmap.Save(context.Server.MapPath(@"~\temp" + filename.Substring(filename.LastIndexOf('.'))));
            byte[] buffer = System.IO.File.ReadAllBytes(context.Server.MapPath(@"~\temp" + filename.Substring(filename.LastIndexOf('.'))));
            response.Clear();
            response.ContentType = "image/jpeg";
            response.OutputStream.Write(buffer, 0, buffer.Length);
            response.End();
        }
        // reusable mode
        public bool IsReusable => true;
    }
}