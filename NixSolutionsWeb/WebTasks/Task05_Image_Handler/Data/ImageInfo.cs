﻿namespace Task05_Image_Handler.Data
{
    // Class that describes image by image name on server and unique id.
    public class ImageInfo
    {
        // image id
        public string Id { get; set; }
        // image name
        public string Imagename { get; set; }

        public ImageInfo(string imageName, string id)
        {
            Imagename = imageName;
            Id = id;
        }
    }
}
