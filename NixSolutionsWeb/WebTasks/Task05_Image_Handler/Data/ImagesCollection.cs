﻿using System.Collections.Generic;

namespace Task05_Image_Handler.Data
{
    // wrapper for images list
    public static class ImagesCollection
    {
        // list of images
        public static List<ImageInfo> Items = new List<ImageInfo>();
    }
}
