﻿/*
    @author Adzhoev M.
*/

using System;
using Task07_TestControl;
using System.Collections.Generic;
using System.Linq;

namespace Task07_TestControlUsage
{
    public partial class Default : System.Web.UI.Page
    {
        // page load event(creates some questions and sets testcontrol1 questions property)
        protected void Page_Load(object sender, EventArgs e)
        {
            TestControl1.TestIsEnded += TestControl1_TestIsEnded;
            if (TestControl1.Questions == null)
            {
                Question q1 = new Question("Вопрос 1? ", new List<Answer> { new Answer("Ответ 1"), new Answer("Ответ 2")
            , new Answer("Ответ 3"), new Answer("Ответ 4")}, 0);
                Question q2 = new Question("Вопрос 2? ", new List<Answer> { new Answer("Ответ 1"), new Answer("Ответ 2")
            , new Answer("Ответ 3"), new Answer("Ответ 4") }, 1);
                Question q3 = new Question("Вопрос 3? "
                    , new List<Answer> { new Answer("Ответ 1"), new Answer("Ответ 2")
            , new Answer("Ответ 3"), new Answer("Ответ 4") }, 2);
                TestControl1.Questions = new List<Question> { q1, q2, q3 };
            }
        }

        // example to show questions information usage(has question text, right answer, selected by user answer)
        private void TestControl1_TestIsEnded(object sender, EventArgs e)
        {
            int counter = 1;
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            foreach (var question in TestControl1.Questions.Where(question => question.IsAnswered))
            {
                lbl.Text += counter + ". " + question.Text + "<br />";
                lbl.Text += "Правильный ответ: " + question.RightAnswer.Text + "<br />";
                lbl.Text += "Указанный ответ: " + question.SelectedAnswer.Text + "<br /> <br />";
                counter++;
            }
            Controls.Add(lbl);
        }
    }
}