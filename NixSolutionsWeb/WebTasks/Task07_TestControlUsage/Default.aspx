﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Task07_TestControlUsage.Default" %>

<%@ Register Assembly="Task07_TestControl" Namespace="Task07_TestControl" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>           
            <cc1:TestControl ID="TestControl1" runat="server" AnswersFontSize="12" QuestionsFontSize="15" />
        </div>
    </form>
</body>
</html>
