﻿/*
    @author Adzhoev M.
*/

using System.Web.UI.WebControls;
namespace Task07_TestControl
{
    // extensions to radio button list control(allows to get and set object which binded with current radio button list)
    public static class RadioButtonListExtensions
    {
        // dictionary with bindings(key - radio button list, value - object to bind)
        private static System.Collections.Generic.Dictionary<RadioButtonList, object> Data = new System.Collections.Generic.Dictionary<RadioButtonList, object>();
        // returns data which binded with current radio button list
        public static object GetData(this RadioButtonList item)
        {
            if (!Data.ContainsKey(item)) return null;
            object value;
            Data.TryGetValue(item, out value);
            return value;
        }
        // binds radio button list with some object
        public static void SetData(this RadioButtonList item, object value)
        {
            Data.Add(item, value);
        }
    }
}
