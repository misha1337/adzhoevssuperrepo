﻿/*
    @author Adzhoev M.
*/

using System;
using System.Collections.Generic;

namespace Task07_TestControl
{
    // describes question with answers list, text and selected, right answers
    [Serializable]
    public class Question
    {
        // right answer property
        public Answer RightAnswer
        {
            get
            {
                return Answers[RightAnswerNumber];
            }
        }
        // selected by user answer property
        public Answer SelectedAnswer
        {
            get
            {
                return Answers[(int)SelectedAnswerNumber];
            }
        }
        // is question answered
        public bool IsAnswered { get; set; }
        // question text(content)
        public string Text { get; set; }
        // answers list
        public List<Answer> Answers;
        // index of right answer
        public int RightAnswerNumber { get; private set; }
        // index of selected by user answer
        public int? SelectedAnswerNumber = null;

        public Question(string text, List<Answer> answers, int rightAnswerNumber)
        {
            Text = text;
            Answers = answers;
            if (rightAnswerNumber < 0 || rightAnswerNumber > answers.Count)
            {
                rightAnswerNumber = 0;
            }
            RightAnswerNumber = rightAnswerNumber;
            answers[RightAnswerNumber].IsRight = true;
        }
    }
}
