﻿/*
    @author Adzhoev M.
*/

namespace Task07_TestControl
{
    // describes answer with text(content) and isRight property
    [System.Serializable]
    public class Answer
    {
        // text(content of answer)
        public string Text { get; set; }
        // is answer right property
        public bool IsRight { get; set; }

        public Answer(string text)
        {
            Text = text;
        }
    }
}
