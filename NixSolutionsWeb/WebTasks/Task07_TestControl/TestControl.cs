﻿/*
    @author Adzhoev M.
*/

using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Task07_TestControl
{
    // control with some test like question with a few answers, also it contains accept test button and information label
    public class TestControl : WebControl
    {
        // amount of answered questions
        private int AnsweredQuestionsAmount
        {
            get
            {
                if (ViewState["aqa"] == null)
                {
                    return 0;
                }
                return (int)ViewState["aqa"];
            }
            set
            {
                ViewState["aqa"] = value;
            }
        }
        // question list
        public List<Question> Questions
        {
            get
            {
                return (List<Question>)ViewState["questions"];
            }
            set
            {
                ViewState["questions"] = value;
            }
        }
        // answers text size
        public int AnswersFontSize
        {
            get; set;
        }
        // questions text size
        public int QuestionsFontSize
        {
            get; set;
        }
        // user event lauched from page when test was ended
        public event EventHandler TestIsEnded;
        // accept test button control
        private Button _acceptButton;
        // information label control(question amount, answered question amount)
        private Label _informationLabel;

        protected override void CreateChildControls()
        {
            foreach (var question in Questions)
            {
                Controls.Add(CreateItemPanel(question));
            }
            _acceptButton = new Button();
            _acceptButton.Click += _acceptButton_Click;
            _acceptButton.Text = "Завершить тест.";
            _informationLabel = new Label();
            _informationLabel.Text = string.Concat("Количество вопросов: ", Questions.Count, " Из них отвечено на: ", AnsweredQuestionsAmount);
            Controls.Add(_acceptButton);
            Controls.Add(new Literal { Text = "<br />" });
            Controls.Add(_informationLabel);
        }
        // accept button click(shows test result information, right answers amount)
        private void _acceptButton_Click(object sender, EventArgs e)
        {
            Controls.Clear();
            int count = Questions.FindAll(g => g.RightAnswerNumber == g.SelectedAnswerNumber).Count;
            Label label = new Label { Text = "Тест успешно пройден. Правильных ответов: " + count } ;
            Controls.Add(label);
            OnSubmitClick(EventArgs.Empty);
        }
        // checks if child controls have been created
        protected override void OnLoad(EventArgs e)
        {
            EnsureChildControls();
        }
        // creates panel with question text and answers radio buttons
        private Panel CreateItemPanel(Question quest)
        {
            HtmlGenericControl div;
            Panel itemPanel = new Panel();

            div = new HtmlGenericControl("div");
            div.Style.Add(HtmlTextWriterStyle.Width, "300px");
            div.Style.Add(HtmlTextWriterStyle.BorderStyle, "solid");
            div.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");

            int size = QuestionsFontSize == 0 ? 14 : QuestionsFontSize;
            Label question = new Label();
            question.Text = quest.Text;
            question.Font.Size = new FontUnit(size);
            div.Controls.Add(question);

            int answerFontSize = AnswersFontSize == 0 ? 8 : AnswersFontSize;
            RadioButtonList answersButtons = new RadioButtonList();        
            answersButtons.AutoPostBack = true;
            answersButtons.Font.Size = new FontUnit(answerFontSize);
            answersButtons.SelectedIndexChanged += AnswersButtons_SelectedIndexChanged;
            answersButtons.SetData(quest);
            foreach (var answer in quest.Answers)
            {
                answersButtons.Items.Add(answer.Text);
            }
            div.Controls.Add(answersButtons);
            if (quest.IsAnswered)
            {
                answersButtons.Items[(int)quest.SelectedAnswerNumber].Attributes.CssStyle.Add(HtmlTextWriterStyle.Color, "green");
            }

            itemPanel.Controls.Add(div);
            return itemPanel;
        }
        // current question answer selected
        private void AnswersButtons_SelectedIndexChanged(object sender, EventArgs e)
        {       
            var radionButtonList = sender as RadioButtonList;
            var question = (Question)radionButtonList.GetData();
            if (!question.IsAnswered)
            {
                AnsweredQuestionsAmount++;
                question.IsAnswered = true;
            }
            foreach (ListItem itm in radionButtonList.Items)
            {
                itm.Attributes.CssStyle[HtmlTextWriterStyle.Color] = "black";
            }
            question.SelectedAnswerNumber = radionButtonList.SelectedIndex;
            radionButtonList.Items[(int)question.SelectedAnswerNumber].Attributes.CssStyle.Add(HtmlTextWriterStyle.Color, "green");
            _informationLabel.Text = string.Concat("Количество вопросов: ", Questions.Count, " Из них отвечено на: ", AnsweredQuestionsAmount);
        }

        
        private void OnSubmitClick(EventArgs e)
        {
            if (TestIsEnded != null)
            {
                TestIsEnded(this, e);
            }
        }
    }
}