﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Task04_Registration_Form
{
    public partial class Default : Page
    {
        #region Fields
        private DropDownList _yearsDropDown; // to convinient datepicker usage (for years selection)
        private DropDownList _monthDropDown; // to convinient datepicker usage (for month selection)
        private Dictionary<string, string> _planetStructureDictionary; // countries - cities collection
        private MultiView _multiView; // main multiView control to add necessary items on it
        private TextBox _nicknameBox;
        private TextBox _nameBox;
        private TextBox _surnameBox;
        private TextBox _birthdayBox; // text box with user birthday value(readonly, takes value from calendar)
        private Calendar _calendar; // sends selected date to birthday box as user birthday
        private DropDownList _countriesList; // drop down list with countries name(takes from dictionary)
        private DropDownList _citiesList; // drop down list with cities name(takes from dictionary by selected item in countries list)
        private TextBox _emailBox; // text box with user e-mail(validates by regular expression)
        #endregion
        // Fills view control to add into MultiView items collection
        private View MainMultiViewCreation()
        {
            var view = new View();
            view.Controls.Add(new ValidationSummary { ShowMessageBox = true, ShowSummary = false });
            // Nickname panel + validators
            view.Controls.Add(CreateControlsPanel(new Label { Text = "Псевдоним: ", SkinID = "lbl" }, _nicknameBox,
                new RequiredFieldValidator { ControlToValidate = "nicknameBox", Text = "*", ErrorMessage = "Invalid nickname.", Display = ValidatorDisplay.Dynamic }));
            // Name panel + validators
            var nameCustomValidator = new CustomValidator
            {
                ErrorMessage = "Имя должно быть отлично от псевдонима.",
                ControlToValidate = "nameBox",
                Text = "Имя должно быть отлично от псевдонима."
            };
            nameCustomValidator.ServerValidate += (source, args) =>
            {
                args.IsValid = _nameBox.Text != _nicknameBox.Text;
            };
            view.Controls.Add(CreateControlsPanel(new Label { Text = "Имя: ", SkinID = "lbl" },
            _nameBox,
                new RequiredFieldValidator
                {
                    ControlToValidate = "nameBox",
                    ErrorMessage = "Invalid name."
                ,
                    Display = ValidatorDisplay.Dynamic,
                    Text = "*"
                }, nameCustomValidator));
            // Surname panel + validators
            var surnameCustomValidator = new CustomValidator
            {
                ErrorMessage = "Фамилия должна быть отлична от имени",
                ControlToValidate = "surnameBox",
                Text = "Фамилия должна быть отлична от имени"
            };
            surnameCustomValidator.ServerValidate += (source, args) =>
            {
                args.IsValid = _surnameBox.Text != _nameBox.Text;
            };
            view.Controls.Add(CreateControlsPanel(new Label { Text = "Фамилия: ", SkinID = "lbl" }, _surnameBox
                , new RequiredFieldValidator
                {
                    ControlToValidate = "surnameBox",
                    ErrorMessage = "Invalid surname."
                ,
                    Display = ValidatorDisplay.Dynamic,
                    Text = "*"
                }, surnameCustomValidator));
            // Birthday date panel + validators
            var birthdayCustomValidator = new CustomValidator
            {
                ErrorMessage = "Дата рождения должна быть в диапазоне от 1 декабря 1960г. до теперешнего времени",
                ControlToValidate = "birthdayBox",
            };
            birthdayCustomValidator.ServerValidate += (source, args) =>
            {
                if (_calendar.SelectedDate < DateTime.Now && _calendar.SelectedDate > new DateTime(1960, 12, 1))
                    args.IsValid = true;
                else
                {
                    args.IsValid = false;
                }
            };
            view.Controls.Add(CreateControlsPanel(new Label { Text = "Дата рождения: ", SkinID = "lbl" }, _birthdayBox, _yearsDropDown, _monthDropDown, _calendar
                , new RequiredFieldValidator
                {
                    ControlToValidate = "birthdayBox",
                    ErrorMessage = "Invalid birthday date."
                ,
                    Display = ValidatorDisplay.Dynamic,
                    Text = "*"
                }, birthdayCustomValidator));
            // Email panel + validators
            view.Controls.Add(CreateControlsPanel(new Label { Text = "E-mail: ", SkinID = "lbl" }, _emailBox, new RegularExpressionValidator
            {
                Text = "*",
                ControlToValidate = "emailBox",
                ErrorMessage = "Неверный формат e-mail",
                ValidationExpression = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            },
                new RequiredFieldValidator
                {
                    ControlToValidate = "emailBox",
                    ErrorMessage = "Invalid email."
                ,
                    Display = ValidatorDisplay.Dynamic,
                    Text = "*"
                }));
            // Country panel
            view.Controls.Add(CreateControlsPanel(new Label { Text = "Страна: ", SkinID = "lbl" }, _countriesList));
            // City panel
            view.Controls.Add(CreateControlsPanel(new Label { Text = "Город: ", SkinID = "lbl" }, _citiesList));
            // Buttons panel
            var resetButton = new Button { Text = "Reset" };
            resetButton.CausesValidation = false;
            resetButton.Click += ResetButtonOnClick;
            var submitButton = new Button { Text = "Submit" };
            submitButton.Click += SubmitButtonOnClick;
            var btnsPanel = new Panel { CssClass = "sbmt" };
            btnsPanel.Controls.Add(resetButton);
            btnsPanel.Controls.Add(submitButton);
            view.Controls.Add(btnsPanel);
            return view;
        }
        // Happends when submit button have been clicked(multiView changes its index for 1, to display another content of current page)
        private void SubmitButtonOnClick(object sender, EventArgs eventArgs)
        {
            if (!Page.IsValid) return;
            _multiView.ActiveViewIndex = 1;
        }
        // Happends when reset button have been clicked(clears all textBoxes on current page)
        private void ResetButtonOnClick(object sender, EventArgs eventArgs)
        {
            Response.Redirect(Request.Url.AbsoluteUri);
        }
        // Creates control panel to add necessary items to view on multiView on page
        private static Panel CreateControlsPanel(params Control[] controls)
        {
            var controlsPanel = new Panel();
            foreach (var control in controls)
            {
                controlsPanel.Controls.Add(control);
            }
            return controlsPanel;
        }
        // Happends when user changed item in countries drop down list
        private void CountriesListOnSelectedIndexChanged(object sender, EventArgs eventArgs)
        {
            if (_countriesList.SelectedItem.Text == "Select a country")
            {
                _citiesList.Items.Clear();
                _citiesList.Enabled = false;
                return;
            }
            _citiesList.Enabled = true;
            _citiesList.Items.Clear();
            var selectedCountry = _countriesList.SelectedValue;
            foreach (var pair in _planetStructureDictionary
                .Where(pair => pair.Value == selectedCountry))
            {
                _citiesList.Items.Add(pair.Key);
            }
        }
        // Inits drop down list with countries names(from dictionary)
        private void InitializeCountriesList(ListControl dropDownList)
        {
            _countriesList.Items.Add("Select a country");
            foreach (var countries in _planetStructureDictionary.Values
                .Where(countries => dropDownList.Items.FindByText(countries) == null))
            {
                dropDownList.Items.Add(new ListItem(countries));
            }

        }
        // Happends when date on a calendar have been changed by user
        private void CalendarOnSelectionChanged(object sender, EventArgs eventArgs)
        {
            _birthdayBox.Text = _calendar.SelectedDate.ToLongDateString();
        }

        // Happends at page initialiation start
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Theme = "MainTheme";
            InitializeControls();
            _multiView = new MultiView();
            _multiView.Views.Add(MainMultiViewCreation());
            var secondView = new View();
            secondView.Controls.Add(new Literal
            {
                Text = "Регистрация прошла успешно."
            });
            _multiView.Views.Add(secondView);
            _multiView.ActiveViewIndex = 0;
            form1.Controls.Add(_multiView);
        }
        // fills monthes dropdown
        protected void PopulateMonthList()
        {
            var dtf = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat;
            for (int i = 1; i <= 12; i++)
            {
                _monthDropDown.Items.Add(new ListItem(dtf.GetMonthName(i), i.ToString()));
            }
            _monthDropDown.Items.FindByValue(DateTime.Now.Month.ToString()).Selected = true;
        }
        // fills years dropdown
        protected void PopulateYearList()
        {
            for (int intYear = 1960; intYear <= DateTime.Now.Year; intYear++)
            {
                _yearsDropDown.Items.Add(intYear.ToString());
            }
            _yearsDropDown.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
        }

        // Inits controls and some their properties or events
        private void InitializeControls()
        {
            _planetStructureDictionary = new Dictionary<string, string>
            {
                { "Kharkiv", "Ukraine" },
                { "Kyiv", "Ukraine" },
                { "Moscow", "Russia" },
                { "Rostov", "Russia" },
                { "Krakow", "Poland" },
                { "Paris", "France" }
            };
            _yearsDropDown = new DropDownList { AutoPostBack = true };
            _yearsDropDown.SelectedIndexChanged += SetCalendar;
            _monthDropDown = new DropDownList { AutoPostBack = true };
            _monthDropDown.SelectedIndexChanged += SetCalendar;
            PopulateYearList();
            PopulateMonthList();
            _nicknameBox = new TextBox { SkinID = "txt", ID = "nicknameBox" };
            _nameBox = new TextBox { SkinID = "txt", ID = "nameBox" };
            _surnameBox = new TextBox { SkinID = "txt", ID = "surnameBox" };
            _emailBox = new TextBox { ID = "emailBox", SkinID = "txt" };
            _birthdayBox = new TextBox { ReadOnly = true, SkinID = "txt", ID = "birthdayBox" };
            _calendar = new Calendar { BorderWidth = 3 };
            _calendar.SelectionChanged += CalendarOnSelectionChanged;
            _countriesList = new DropDownList { AutoPostBack = true };
            _countriesList.DataValueField = "";
            InitializeCountriesList(_countriesList);
            _countriesList.SelectedIndexChanged += CountriesListOnSelectedIndexChanged;
            _citiesList = new DropDownList { Enabled = false };
        }
        // happends when year or month drop down selection changed
        protected void SetCalendar(object Sender, EventArgs e)
        {
            int year = int.Parse(_yearsDropDown.SelectedValue);
            int month = int.Parse(_monthDropDown.SelectedValue);
            _calendar.TodaysDate = new DateTime(year, month, 1);
        }
    }
}