﻿using System;

namespace Task07_Folders_Structure.Model
{
    // To describe operation system element(like file or folder)
    internal class SystemElement
    {
        // Element name
        public string Name { get; set; }
        // Element length(if it's folder than equals 0)
        public long Length { get; set; }
        // Element creation time
        public DateTime CreationTime { get; set; }

        public SystemElement(string name, DateTime creationTime, long length = 0)
        {
            Name = name;
            Length = length;
            CreationTime = creationTime;
        }

        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
