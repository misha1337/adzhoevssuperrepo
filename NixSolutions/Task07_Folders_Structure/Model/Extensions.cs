﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Task07_Folders_Structure.Model
{
    // Static class with extension methods
    internal static class Extensions
    {
        // Extension method for classes which implements
        // ISynchronizeInvoke to post message into text box about some runtime exception
        public static void PostMessage(this ISynchronizeInvoke synchronizeInvoke, TextBox textBox, string message)
        {
            textBox.AppendText(message + Environment.NewLine);
        }
    }
}
