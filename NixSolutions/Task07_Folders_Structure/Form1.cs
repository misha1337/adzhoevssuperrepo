﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using Task07_Folders_Structure.Model;

namespace Task07_Folders_Structure
{
    // Main form of current application
    public partial class Form1 : Form
    {
        #region Fields
        private readonly ContextMenuStrip _contextMenuStrip;
        // Document for handling in the second thread
        private readonly XmlDocument _document;
        // Collection of nodes
        private readonly List<SystemElement> _list = new List<SystemElement>();
        // Counter for walking list in tree view builder thread
        private int _firstThreadCounter;
        // Counter for walking list in xml file builder thread
        private int _secondThreadCounter;
        // Path to xml file where system(file-folder) structure will be written
        private string _pathToXmlFile;
        // Cap for making synchronization amoung threads
        private readonly object _locker = new object();
        // Thread for catching file system structure
        private readonly Thread _informationCatcherThread;
        // Thread for build xml file
        private readonly Thread _xmlBuilderThread;
        // Thread for build treeView control
        private readonly Thread _treeViewBuilderThread;
        // Event handler to informate user about exceptions which were catched during information handling
        private event EventHandler<string> ExceptionCatchedHandler;
        #endregion
        // Main form initializer
        public Form1()
        {
            InitializeComponent();
            _document = new XmlDocument();
            _contextMenuStrip = new ContextMenuStrip();
            _contextMenuStrip.Items.Add(new ToolStripButton("Open"));
            _contextMenuStrip.ItemClicked += ContextMenuStrip_ItemClicked;
            _informationCatcherThread = new Thread(GetSystemStructure)
            {
                IsBackground = true,
                Name = "Поток сбора информации"
            };
            _xmlBuilderThread = new Thread(XmlFileBuilder)
            {
                IsBackground = true,
                Name = "Поток построения древовидной иерархии в XMl файле"
            };
            _treeViewBuilderThread = new Thread(TreeViewBuilder)
            {
                IsBackground = true,
                Name = "Поток заполнения TreeView контрола"
            };
            _xmlBuilderThread.Start();
            _treeViewBuilderThread.Start();
            ExceptionCatchedHandler += OnEventHandler;
        }
        // Event handler invocator which invokes method to post message in textbox control on main form
        private void OnEventHandler(object sender, string e)
        {
            try
            {
                Invoke(new Action<TextBox, string>(this.PostMessage), exceptionsBox, e);
            }
            catch { }
        }
        // Handles event when main form are closing
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _informationCatcherThread.Abort();
            _xmlBuilderThread.Abort();
            _treeViewBuilderThread.Abort();
        }
        // handles event when button START on main form were clicked
        private void button1_Click(object sender, EventArgs e)
        {
            var folderBrowser = new FolderBrowserDialog { Description = @"Выбери директорию для начала работы." };
            var saveFileDialog = new SaveFileDialog();
            folderBrowser.ShowDialog();
            var selectedFolderForHandle = folderBrowser.SelectedPath;
            saveFileDialog.ShowDialog();
            _pathToXmlFile = saveFileDialog.FileName;
            if (selectedFolderForHandle == "" || _pathToXmlFile == "")
            {
                return;
            }
            _informationCatcherThread.Start(new DirectoryInfo(selectedFolderForHandle));
            button1.Enabled = false;
        }
        // Handles event when treeView control item has been selected
        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var item = treeView.SelectedNode.FullPath;
            var searched = _list.Find(element => element.Name == item);
            if (searched == null)
            {
                nameBox.Text = treeView.SelectedNode.FullPath;
                lengthBox.Text = "";
                creationBox.Text = "";
            }
            else
            {
                nameBox.Text = searched.Name.Substring(searched.Name.LastIndexOf(Path.DirectorySeparatorChar) + 1);
                lengthBox.Text = string.Concat(searched.Length.ToString(), " байт");
                creationBox.Text = searched.CreationTime.ToLongDateString();
            }
        }
        #region MainLogicOfAnApplication
        // Get system files and folders structure to list
        private void GetSystemStructure(object item)
        {
            var directoryInfo = item as DirectoryInfo;
            try
            {
                if (directoryInfo == null)
                {
                    return;
                }
                lock (_locker)
                {
                    _list.Add(new SystemElement(directoryInfo.FullName, directoryInfo.CreationTime));
                }
                foreach (var file in directoryInfo.GetFiles())
                {
                    lock (_locker)
                    {
                        _list.Add(new SystemElement(file.FullName, file.CreationTime,file.Length));
                    }
                }
                foreach (var director in directoryInfo.GetDirectories())
                {
                    GetSystemStructure(director);
                }
            }
            catch (Exception exc)
            {
                ExceptionCatchedHandler?.Invoke(this,
                        string.Concat(exc.Message, Environment.NewLine, "Имя потока: ", Thread.CurrentThread.Name));
            }
        }
        // Builds treeView control by taking information from list with pathes to necessary directory
        private void TreeViewBuilder()
        {
            while (true)
            {
                try
                {
                    if (_list.Count <= _firstThreadCounter)
                    {
                        continue;
                    }
                    lock (_locker)
                    {
                        var item = _list[_firstThreadCounter++];
                        var childs = treeView.Nodes;
                        var path = item.Name.Split(Path.DirectorySeparatorChar);
                        path.Aggregate(childs, (current, part) => FindOrCreateNode(current, part, item).Nodes);
                    }
                }
                catch (Exception exc)
                {
                    ExceptionCatchedHandler?.Invoke(this,
                        string.Concat(exc.Message, Environment.NewLine, "Имя потока: ", Thread.CurrentThread.Name));
                }
            }
        }
        // Finds or creates node in treeNodeCollection from current level
        private TreeNode FindOrCreateNode(TreeNodeCollection coll, string name, SystemElement element)
        {
            var found = coll.Find(name.ToLower(), false);
            Func<TreeNodeCollection, string, SystemElement, TreeNode> func = (collection, itemName, elementName) =>
            {
                var node = collection.Add(name.ToLower(), name);
                
                node.Tag = element;
                return node;
            };
            return (TreeNode)(found.Length > 0 ? found[0] : Invoke(func, coll, name, element));
        }
        // Builds xml file from list with pathes
        private void XmlFileBuilder()
        {
            while (true)
            {
                try
                {
                    if (_list.Count <= _secondThreadCounter)
                    {
                        continue;
                    }
                    lock (_locker)
                    {
                        XmlNode child = _document;
                        var path = _list[_secondThreadCounter++].Name.Split(Path.DirectorySeparatorChar);
                        path.Aggregate(child, (current, part) => FindOrCreateNode(_document, current, part));
                        _document.Save(_pathToXmlFile);
                    }
                }
                catch (Exception exc)
                {
                    ExceptionCatchedHandler?.Invoke(this,
                        string.Concat(exc.Message, Environment.NewLine, "Имя потока: ", Thread.CurrentThread.Name));
                }
            }
        }
        // Finds or creates xml node in XmlDocument
        private static XmlNode FindOrCreateNode(XmlDocument doc, XmlNode root, string name)
        {
            foreach (var node in root.ChildNodes.Cast<XmlNode>()
                .Where(node => node.Attributes != null && node.Attributes["id"].Value == name))
            {
                return node;
            }
            var result = doc.CreateNode(XmlNodeType.Element, "SystemElement", "");
            result.Attributes?.Append(doc.CreateAttribute("id"));
            if (result.Attributes != null)
            {
                result.Attributes["id"].Value = name;
            }
            root.AppendChild(result);
            return result;
        }
        // Other features
        private void ContextMenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                var systemElement = treeView.SelectedNode.Tag as SystemElement;
                if (systemElement == null) return;
                Process.Start(systemElement.Name);
            }
            catch (Exception exc)
            {
                ExceptionCatchedHandler?.Invoke(this,
                        string.Concat(exc.Message, Environment.NewLine, "Error in file opening"));
            }
        }

        private void treeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;
            treeView.SelectedNode = e.Node;
            _contextMenuStrip.Show(treeView, e.Location);
        }
        #endregion

    }
}
