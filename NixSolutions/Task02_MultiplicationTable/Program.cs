﻿/**
* Task: Вывести на консоль таблицу умножения
@author Adzhoev Mikhail
*/

using System;

namespace Task02_MultiplicationTable
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("for: ");
            ForCircle();
            Console.WriteLine("while: ");
            WhileCircle();
            Console.WriteLine("do..while: ");
            DoWhileCircle();
            Console.WriteLine("foreach: ");
            ForEachCircle();
        }
        /// <summary>
        /// Вывод на экран результата умножения чисел(с форматированием)
        /// </summary>
        static void Print(int i, int j)
        {
            if (i == j) Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("{0, -2} ", i * j);
            Console.ResetColor();
        }
        /// <summary>
        /// Вывод таблицы умножения при помощи цикла For
        /// </summary>
        static void ForCircle()
        {
            for (var i = 1; i < 11; i++)
            {
                for (var j = 1; j < 11; j++)
                    Print(i, j);
                Console.WriteLine();
            }
        }
        /// <summary>
        /// Вывод таблицы умножения при помощи цикла While
        /// </summary>
        static void WhileCircle()
        {
            var i = 1;
            while (i <= 10)
            {
                var j = 1;
                while (j <= 10)
                {
                    Print(i, j);
                    j++;
                }
                Console.WriteLine();
                i++;
            }
        }
        /// <summary>
        /// Вывод таблицы умножения при помощи цикла DoWhile
        /// </summary>
        static void DoWhileCircle()
        {
            var i = 1;
            do
            {
                var j = 1;
                do
                {
                    Print(i, j);
                    j++;
                } while (j <= 10);
                Console.WriteLine();
                i++;
            } while (i <= 10);
        }
        /// <summary>
        /// Вывод таблицы умножения при помощи цикла ForEach
        /// </summary>
        static void ForEachCircle()
        {
            var numbers = new int[10];
            for (var i = 1; i < 11; i++)
                numbers[i - 1] = i;
            foreach (var number in numbers)
            {
                foreach (var secnumber in numbers)
                    Print(number, secnumber);
                Console.WriteLine();
            }
        }
    }
}

