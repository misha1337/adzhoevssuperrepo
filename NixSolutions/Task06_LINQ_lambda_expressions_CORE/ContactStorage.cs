﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Task06_LINQ_lambda_expressions_CORE
{
    // Contact information storage based on xml file
    public class ContactStorage
    {
        // Xml document as storage link
        private readonly XDocument _document;
        // File name of storage
        private readonly string _storageFileName;
        public ContactStorage(string storageName)
        {
            _storageFileName = storageName;
            _document = XDocument.Load(_storageFileName);
        }
        // Getting all the contacts from storage
        public IEnumerable<Contact> GetAll()
        {
            return from contact in _document.Descendants("Contact")
                   select new Contact
                   {
                       Name = contact.Element("Name").Value,
                       Surname = contact.Element("Surname").Value,
                       Group = contact.Element("Group").Value,
                       Number = contact.Element("Number").Value,
                       MobileNumber = contact.Element("MobileNumber").Value,
                       Photo = contact.Element("Photo").Value,
                   };
        }
        // Add new contact to xml file
        public void Add(Contact contact)
        {
            var xElement = _document.Element("Contacts");
            xElement?.Add(
                new XElement("Contact", new XAttribute("name", contact.Name + contact.Surname),
                    new XElement("Name", contact.Name),
                    new XElement("Surname", contact.Surname),
                    new XElement("Group", contact.Group),
                    new XElement("Number", contact.Number),
                    new XElement("MobileNumber", contact.MobileNumber),
                    new XElement("Photo", contact.Photo)
                    ));
            _document.Save(_storageFileName);
        }

        // Remove contact from xml file
        public void Remove(Contact contact)
        {
            _document.Descendants("Contact")
                .Where(x => (string)x.Attribute("name") == (contact.Name + contact.Surname))
                .Remove();
            _document.Save(_storageFileName);
        }
        // Change contact in xml file
        public void Update(Contact toChangeContact, Contact changer)
        {
            var target = _document
                .Descendants("Contact")
                .Single(e => (string)e.Attribute("name") == (toChangeContact.Name + toChangeContact.Surname));
            target.Element("Name").Value = changer.Name;
            target.Element("Surname").Value = changer.Surname;
            target.Element("Group").Value = changer.Group;
            target.Element("Number").Value = changer.Number;
            target.Element("MobileNumber").Value = changer.MobileNumber;
            target.Element("Photo").Value = changer.Photo;
            target.Attribute("name").Value = changer.Name + changer.Surname;
            _document.Save(_storageFileName);
        }
        // Finds contact which name and surname contains query
        public IEnumerable<Contact> FindByNameAndSurname(string query)
        {
            return from item in GetAll()
                   where item.ToString().ToLower().Contains(query.ToLower())
                   select item;
        }
    }
}
