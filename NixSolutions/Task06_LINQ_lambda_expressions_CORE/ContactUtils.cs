﻿using System.Collections.Generic;
using System.Linq;

namespace Task06_LINQ_lambda_expressions_CORE
{
    // Utility class for contacts handling
    public static class ContactUtils
    {
        // Return enumerable of contacts groupped by group field
        public static IEnumerable<IGrouping<string, Contact>> GetGroppedContacts(IEnumerable<Contact> contacts)
        {
            return contacts.GroupBy(g => g.Group);
        }
    }
}
