﻿namespace Task06_LINQ_lambda_expressions_CORE
{
    // Class for describing "Contact"
    public class Contact
    {
        // Contact first name
        public string Name { get; set; }
        // Contact second name
        public string Surname { get; set; }
        // Contact group(i.e family, neighbours, job, friends..)
        public string Group { get; set; }
        // Contact home number
        public string Number { get; set; }
        // Contact mobile number
        public string MobileNumber { get; set; }
        // Contact photo link
        public string Photo { get; set; }
        // ToStringMethod overriding
        public override string ToString()
        {
            return $"{Name} {Surname}";
        }
    }
}
