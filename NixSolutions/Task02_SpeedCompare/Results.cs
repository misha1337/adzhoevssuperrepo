﻿using System;

namespace Task02_SpeedCompare
{
    /// <summary>
    /// Структура с результатами замерок
    /// </summary>
    struct Results
    {
        public double ListAdditionSpeed;
        public double ArrayListAdditionSpeed;
        public double ListGetSpeed;
        public double ArrayListGetSpeed;
        public double ListSortSpeed;
        public double ArrayListSortSpeed;
        /// <summary>
        /// Вывод результатов измерений на консоль
        /// </summary>
        public void Print()
        {
            Console.WriteLine("Скорость добавления int в List<> : " + ListAdditionSpeed + " ms");
            Console.WriteLine("Скорость добавления int в ArrayList : " + ArrayListAdditionSpeed + " ms");
            Console.WriteLine("Скорость получения последнего элемента в List<> : " + ListGetSpeed + " ms");
            Console.WriteLine("Скорость получения последнего элемента в ArrayList : " + ArrayListGetSpeed + " ms");
            Console.WriteLine("Скорость сортировки List<> : " + ListSortSpeed + " ms");
            Console.WriteLine("Скорость сортировки ArrayList : " + ArrayListSortSpeed + " ms");
        }
    }
}
