﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Task02_SpeedCompare
{
    class Program
    {
        static void Main(string[] args)
        {
            Results intResults = new Results();
            Results stringResults = new Results();
            ArrayList arrayList = new ArrayList();
            // INTS
            Console.WriteLine("INTEGER32: ");
            List<int> ints = new List<int>();
            intResults.ListAdditionSpeed = SpeedCalculator.CalculateAddSpeed(ints, Generator.GenerateInts(100000));
            intResults.ArrayListAdditionSpeed = SpeedCalculator.CalculateAddSpeed(arrayList, Generator.GenerateInts(100000));
            intResults.ListGetSpeed = SpeedCalculator.CalculateGetSpeed(ints, ints.Count-1);
            intResults.ArrayListGetSpeed = SpeedCalculator.CalculateGetSpeed(arrayList, arrayList.Count-1);
            intResults.ListSortSpeed = SpeedCalculator.CalculateSortListSpeed(ints);
            intResults.ArrayListSortSpeed = SpeedCalculator.CalculateSortArrayListSpeed(arrayList);
            intResults.Print();
            // STRINGS
            Console.WriteLine("STRING: ");
            List<string> strings = new List<string>();
            stringResults.ListAdditionSpeed = SpeedCalculator.CalculateAddSpeed(strings, Generator.GenerateStrings(100000));
            arrayList.Clear();
            stringResults.ArrayListAdditionSpeed = SpeedCalculator.CalculateAddSpeed(arrayList, Generator.GenerateStrings(100000));
            stringResults.ListGetSpeed = SpeedCalculator.CalculateGetSpeed(strings, strings.Count-1);
            stringResults.ArrayListGetSpeed = SpeedCalculator.CalculateGetSpeed(arrayList, arrayList.Count-1);
            stringResults.ListSortSpeed = SpeedCalculator.CalculateSortListSpeed(strings);
            stringResults.ArrayListSortSpeed = SpeedCalculator.CalculateSortArrayListSpeed(arrayList);
            stringResults.Print();
        }
    }
}

