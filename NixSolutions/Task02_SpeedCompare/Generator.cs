﻿using System;

namespace Task02_SpeedCompare
{
    /// <summary>
    /// Генератор массивов строк и чисел
    /// </summary>
    static class Generator
    {
        private static Random rnd = new Random();
        /// <summary>
        /// Генератор массива чисел
        /// </summary>
        /// <param name="size">Необходимый размер массива</param>
        /// <returns>Сгенерированный массив</returns>
        public static int[] GenerateInts(int size)
        {
            int[] ints = new int[size];
            for (var i = 0; i < size; i++)
                ints[i] = rnd.Next();
            return ints;
        }
        /// <summary>
        /// Генератор массива строк
        /// </summary>
        /// <param name="size">Необходимый размер массива</param>
        /// <returns>Сгенерированный массив</returns>
        public static string[] GenerateStrings(int size)
        {
            string[] strings = new string[size];
            for (var i = 0; i < size; i++)
                strings[i] = rnd.Next().ToString();
            return strings;
        }
    }
}
