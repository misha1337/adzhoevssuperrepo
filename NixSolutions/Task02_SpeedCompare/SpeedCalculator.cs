﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Task02_SpeedCompare
{
    /// <summary>
    /// Расчет скоростей добавления и получения эл-тов коллекции
    /// </summary>
    static class SpeedCalculator
    {
        private static Stopwatch sw = new Stopwatch();
        /// <summary>
        /// Расчет скорости добавления в коллекцию
        /// </summary>
        /// <typeparam name="T">Тип элементов в коллекции</typeparam>
        /// <param name="list">Коллекция для подсчета</param>
        /// <param name="toAddArray">Значения для добавления</param>
        /// /// <returns></returns>
        public static double CalculateAddSpeed<T>(IList list, T[] toAddArray)
        {
            sw.Start();
            for (var i = 0; i < toAddArray.Length; i++)
                list.Add(toAddArray[i]);
            sw.Stop();
            return sw.Elapsed.TotalMilliseconds;
        }
        /// <summary>
        /// Расчет скорости получения элемента из коллекции
        /// </summary>
        /// <param name="list">Коллекция для расчета</param>
        /// <param name="index">Индекс для получения</param>
        /// <returns></returns>
        public static double CalculateGetSpeed(IList list, int index)
        {
            sw.Restart();
            for (var i = 0; i < index; i++)
            {
                var item = list[i];
            }
            sw.Stop();
            return sw.Elapsed.TotalMilliseconds;
        }
        /// <summary>
        /// Расчет скорости сортировки дженерик листа
        /// </summary>
        /// <typeparam name="T">Тип данных в списке</typeparam>
        /// <param name="list">Список для расчета</param>
        /// <returns></returns>
        public static double CalculateSortListSpeed<T>(List<T> list)
        {
            sw.Restart();
            list.Sort();
            sw.Stop();
            return sw.Elapsed.TotalMilliseconds;
        }
        /// <summary>
        /// Расчет скорости сортировки ArrayList
        /// </summary>
        /// <param name="list">Коллекция для сортировки</param>
        /// <returns></returns>
        public static double CalculateSortArrayListSpeed(ArrayList list)
        {
            sw.Restart();
            list.Sort();
            sw.Stop();
            return sw.Elapsed.TotalMilliseconds;
        }
    }
}
