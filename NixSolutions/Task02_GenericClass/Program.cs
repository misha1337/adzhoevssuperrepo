﻿using System;
using System.Collections.Generic;

namespace Task02_GenericClass
{
    class Program
    {
        /// <summary>
        /// Вывод содержимого списка
        /// </summary>
        /// <param name="gc">список для вывода</param>
        private static void PrintList<T>(List<GenericClass<T>> gc) where T : struct, IComparable<T>
        {
            foreach (var generic in gc)
            {
                Console.WriteLine(generic.SomeProperty);
            }
        }
        static void Main(string[] args)
        {
            List<GenericClass<char>> genericClasses1 = new List<GenericClass<char>>
            {
                new GenericClass<char>('b'),
                new GenericClass<char>('a'),
                new GenericClass<char>('z'),
                new GenericClass<char>('c'),
                new GenericClass<char>('g'),
                new GenericClass<char>('h')
            };
            Console.WriteLine("CHAR: ");
            Console.WriteLine("До: ");
            PrintList(genericClasses1);
            genericClasses1.Sort();
            Console.WriteLine("После: ");
            PrintList(genericClasses1);

            List<GenericClass<double>> genericClasses2 = new List<GenericClass<double>>
            {
                new GenericClass<double>(31.12),
                new GenericClass<double>(8.22),
                new GenericClass<double>(14.08),
                new GenericClass<double>(5),
                new GenericClass<double>(11.321),
                new GenericClass<double>(109.6)
            };
            Console.WriteLine("DOUBLE: ");
            Console.WriteLine("До: ");
            PrintList(genericClasses2);
            genericClasses2.Sort();
            Console.WriteLine("После: ");
            PrintList(genericClasses2);
        }
    }
}

