﻿using System;

namespace Task02_GenericClass
{
    /// <summary>
    /// Дженерик класс параметром которого может быть только тип значения
    /// </summary>
    /// <typeparam name="T">Параметр типа значения</typeparam>
    class GenericClass<T> : IComparable<GenericClass<T>> where T : struct, IComparable<T>
    {
        // Публичное свойство типа значения
        public T SomeProperty { get; set; }
        /// <summary>
        /// Конструктор с параметром
        /// </summary>
        /// <param name="value">Значение для публичного свойства</param>
        public GenericClass(T value)
        {
            SomeProperty = value;
        }
        /// <summary>
        /// Сравнение элементов для сортировки
        /// </summary>
        /// <returns>1 - больше</returns>
        /// <returns>-1 - меньше</returns>
        /// <returns>0 - равны</returns>
        public int CompareTo(GenericClass<T> other)
        {
            return SomeProperty.CompareTo(other.SomeProperty);
        }
    }
}

