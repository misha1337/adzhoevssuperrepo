﻿using System;
using System.Windows.Forms;
using Task06_LINQ_lambda_expressions_UI.Forms;

namespace Task06_LINQ_lambda_expressions_UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
