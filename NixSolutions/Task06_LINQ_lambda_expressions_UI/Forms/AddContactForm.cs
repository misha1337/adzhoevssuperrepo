﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Task06_LINQ_lambda_expressions_CORE;

namespace Task06_LINQ_lambda_expressions_UI.Forms
{
    // Form for add and edit contacts of storage
    public partial class AddContactForm : Form
    {
        private readonly ContactStorage _contactStorage;
        private readonly Contact _editableContact;
        // Constructor in case when contact should be added
        public AddContactForm(ContactStorage contactStorage)
        {
            InitializeComponent();
            _contactStorage = contactStorage;
        }
        // Constructor in case when contact should be updated
        public AddContactForm(Contact contact, ContactStorage contactStorage)
        {
            InitializeComponent();
            _contactStorage = contactStorage;
            _editableContact = contact;
            nameBox.Text = contact.Name;
            surnameBox.Text = contact.Surname;
            groupBox.Text = contact.Group;
            numberBox.Text = contact.Number;
            mobBox.Text = contact.MobileNumber;
            photoBox.Text = contact.Photo;
        }
        // Accept add or edit button click handler
        private void acceptButton_Click(object sender, EventArgs e)
        {
            var cont = CreateContact();
            if (cont == null) return;
            if (_editableContact == null)
            {
                _contactStorage.Add(cont);
            }
            else
            {
                _contactStorage.Update(_editableContact, cont);
            }
            Close();
        }
        // Creats a new contact by the values of textBoxes(with validation)
        private Contact CreateContact()
        {
            inputFailsBox.Text = "";
            var valid = true;
            var contact = new Contact();
            if (IsValid(nameBox.Text, "^[A-ZА-ЯЁ][a-zа-яё]+$"))
            {
                nameBox.BackColor = System.Drawing.Color.Green;
                contact.Name = nameBox.Text;
            }
            else
            {
                nameBox.BackColor = System.Drawing.Color.Red;
                contact.Name = nameBox.Text;
                inputFailsBox.AppendText(string.Concat("Invalid name", Environment.NewLine));
                valid = false;
            }
            if (IsValid(surnameBox.Text, "^[A-ZА-ЯЁ][a-zа-яё]+$"))
            {
                contact.Surname = surnameBox.Text;
                surnameBox.BackColor = System.Drawing.Color.Green;
            }
            else
            {
                surnameBox.BackColor = System.Drawing.Color.Red;
                contact.Surname = surnameBox.Text;
                inputFailsBox.AppendText(string.Concat("Invalid surname", Environment.NewLine));
                valid = false;
            }
            if (!string.IsNullOrWhiteSpace(groupBox.Text))
            {
                contact.Group = groupBox.Text;
                groupBox.BackColor = System.Drawing.Color.Green;
            }
            else
            {
                groupBox.BackColor = System.Drawing.Color.Red;
                contact.Group = groupBox.Text;
                inputFailsBox.AppendText(string.Concat("Invalid group", Environment.NewLine));
                valid = false;
            }
            if (IsValid(numberBox.Text, "^[0-9]+$"))
            {
                contact.Number = numberBox.Text;
                numberBox.BackColor = System.Drawing.Color.Green;
            }
            else
            {
                numberBox.BackColor = System.Drawing.Color.Red;
                contact.Number = numberBox.Text;
                inputFailsBox.AppendText(string.Concat("Invalid number", Environment.NewLine));
                valid = false;
            }
            if (IsValid(mobBox.Text, "^[0-9]+$"))
            {
                contact.MobileNumber = mobBox.Text;
                mobBox.BackColor = System.Drawing.Color.Green;
            }
            else
            {
                mobBox.BackColor = System.Drawing.Color.Red;
                contact.MobileNumber = mobBox.Text;
                inputFailsBox.AppendText(string.Concat("Invalid mobile number", Environment.NewLine));
                valid = false;
            }
            contact.Photo = photoBox.Text;
            return valid ? contact : null;
        }
        // Checks if entered field is valid
        private static bool IsValid(string text, string pattern)
        {
            return Regex.IsMatch(text, pattern);
        }

        // Opens file dialog to select a contact picture
        private void fileDialogButton_Click(object sender, EventArgs e)
        {
            var fileDialog = new OpenFileDialog { InitialDirectory = "../Resources/" };

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                photoBox.Text = fileDialog.FileName;
            }
        }
    }
}
