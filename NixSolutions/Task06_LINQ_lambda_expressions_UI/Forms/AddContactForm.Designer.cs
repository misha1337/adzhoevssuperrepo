﻿namespace Task06_LINQ_lambda_expressions_UI.Forms
{
    partial class AddContactForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddContactForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.surnameBox = new System.Windows.Forms.TextBox();
            this.groupBox = new System.Windows.Forms.TextBox();
            this.numberBox = new System.Windows.Forms.TextBox();
            this.mobBox = new System.Windows.Forms.TextBox();
            this.photoBox = new System.Windows.Forms.TextBox();
            this.acceptButton = new System.Windows.Forms.Button();
            this.fileDialogButton = new System.Windows.Forms.Button();
            this.inputFailsBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Surname:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Group:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Number:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Mobile number:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Photo link:";
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(88, 3);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(375, 20);
            this.nameBox.TabIndex = 6;
            // 
            // surnameBox
            // 
            this.surnameBox.Location = new System.Drawing.Point(88, 29);
            this.surnameBox.Name = "surnameBox";
            this.surnameBox.Size = new System.Drawing.Size(375, 20);
            this.surnameBox.TabIndex = 7;
            // 
            // groupBox
            // 
            this.groupBox.Location = new System.Drawing.Point(88, 55);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(375, 20);
            this.groupBox.TabIndex = 8;
            // 
            // numberBox
            // 
            this.numberBox.Location = new System.Drawing.Point(88, 81);
            this.numberBox.Name = "numberBox";
            this.numberBox.Size = new System.Drawing.Size(375, 20);
            this.numberBox.TabIndex = 9;
            // 
            // mobBox
            // 
            this.mobBox.Location = new System.Drawing.Point(88, 107);
            this.mobBox.Name = "mobBox";
            this.mobBox.Size = new System.Drawing.Size(375, 20);
            this.mobBox.TabIndex = 10;
            // 
            // photoBox
            // 
            this.photoBox.Location = new System.Drawing.Point(88, 146);
            this.photoBox.Name = "photoBox";
            this.photoBox.ReadOnly = true;
            this.photoBox.Size = new System.Drawing.Size(336, 20);
            this.photoBox.TabIndex = 11;
            // 
            // acceptButton
            // 
            this.acceptButton.Location = new System.Drawing.Point(123, 188);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(266, 23);
            this.acceptButton.TabIndex = 12;
            this.acceptButton.Text = "Accept";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.acceptButton_Click);
            // 
            // fileDialogButton
            // 
            this.fileDialogButton.Location = new System.Drawing.Point(431, 144);
            this.fileDialogButton.Name = "fileDialogButton";
            this.fileDialogButton.Size = new System.Drawing.Size(32, 23);
            this.fileDialogButton.TabIndex = 13;
            this.fileDialogButton.Text = "..";
            this.fileDialogButton.UseVisualStyleBackColor = true;
            this.fileDialogButton.Click += new System.EventHandler(this.fileDialogButton_Click);
            // 
            // inputFailsBox
            // 
            this.inputFailsBox.Location = new System.Drawing.Point(482, 3);
            this.inputFailsBox.Multiline = true;
            this.inputFailsBox.Name = "inputFailsBox";
            this.inputFailsBox.ReadOnly = true;
            this.inputFailsBox.Size = new System.Drawing.Size(152, 208);
            this.inputFailsBox.TabIndex = 14;
            // 
            // AddContactForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.CadetBlue;
            this.ClientSize = new System.Drawing.Size(646, 235);
            this.Controls.Add(this.inputFailsBox);
            this.Controls.Add(this.fileDialogButton);
            this.Controls.Add(this.acceptButton);
            this.Controls.Add(this.photoBox);
            this.Controls.Add(this.mobBox);
            this.Controls.Add(this.numberBox);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.surnameBox);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddContactForm";
            this.Text = "Add contact";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.TextBox surnameBox;
        private System.Windows.Forms.TextBox groupBox;
        private System.Windows.Forms.TextBox numberBox;
        private System.Windows.Forms.TextBox mobBox;
        private System.Windows.Forms.TextBox photoBox;
        private System.Windows.Forms.Button acceptButton;
        private System.Windows.Forms.Button fileDialogButton;
        private System.Windows.Forms.TextBox inputFailsBox;
    }
}