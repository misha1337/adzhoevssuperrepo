﻿using System;
using System.Configuration;
using System.Drawing;
using System.Windows.Forms;
using Task06_LINQ_lambda_expressions_CORE;

namespace Task06_LINQ_lambda_expressions_UI.Forms
{
    // Main form of Contact manager application
    public partial class Form1 : Form
    {
        // Contact storage based on xml file
        public ContactStorage ContactStorage;

        public Form1()
        {
            InitializeComponent();
            var appSettings = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings;
            ContactStorage = new ContactStorage(appSettings["fileName"].Value);
            LoadStorage();
        }
        // Add or edit button click handler
        private void addButton_Click(object sender, EventArgs e)
        {
            ClearFields();
            if (listBox.SelectedItems.Count == 0)
            {
                if (treeView.SelectedNode?.Parent != null)
                {
                    var node = treeView.SelectedNode;
                    if (node.Nodes.Count != 0)
                    {
                        return;
                    }
                    var editForm = new AddContactForm(node.Tag as Contact, ContactStorage);
                    editForm.ShowDialog();
                    return;
                }
                var addForm = new AddContactForm(ContactStorage);
                addForm.ShowDialog();
            }
            else
            {
                var editForm = new AddContactForm(listBox.SelectedItem as Contact, ContactStorage);
                editForm.ShowDialog();
            }
            LoadStorage();
            LoadTreeView();
        }
        // Loads storage to listbox
        private void LoadStorage()
        {
            listBox.Items.Clear();
            foreach (var item in ContactStorage.GetAll())
            {
                listBox.Items.Add(item);
            }
        }
        // Loads storage to tree view
        private void LoadTreeView()
        {
            treeView.Nodes.Clear();
            var index = 0;
            foreach (var group in ContactUtils.GetGroppedContacts(ContactStorage.GetAll()))
            {
                treeView.Nodes.Add(group.Key);
                foreach (var gr in group)
                {
                    var node = treeView.Nodes[index].Nodes.Add(gr.ToString());
                    node.Tag = gr;
                }
                index++;
            }
            treeView.ExpandAll();
        }
        // Delete selected item button click handler
        private void deleteButton_Click(object sender, EventArgs e)
        {
            Contact toDeleteContact;
            if (listBox.SelectedItems.Count == 0)
            {
                if (treeView.SelectedNode?.Parent != null)
                {
                    var node = treeView.SelectedNode;
                    if (node.Nodes.Count != 0)
                    {
                        return;
                    }
                    toDeleteContact = node.Tag as Contact;
                }
                else
                {
                    return;
                }
            }
            else
            {
                toDeleteContact = listBox.SelectedItem as Contact;
            }
            ClearFields();
            ContactStorage.Remove(toDeleteContact);
            LoadStorage();
            LoadTreeView();
        }
        // Handles event when item in a listbox were selected
        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var item = (sender as ListBox)?.SelectedItem as Contact;
            if (item == null) return;
            ShowContact(item);
        }
        // Launches fields on form to show selected contact info
        private void ShowContact(Contact item)
        {
            Bitmap mp;
            nameBox.Text = item.Name;
            surnameBox.Text = item.Surname;
            groupBox.Text = item.Group;
            numberBox.Text = item.Number;
            mobileBox.Text = item.MobileNumber;
            pictureBox.SizeMode = PictureBoxSizeMode.CenterImage;
            try
            {
                mp = new Bitmap(Image.FromFile(item.Photo), new Size(112, 124));
            }
            catch
            {
                mp = new Bitmap(Properties.Resources.Default, new Size(112, 124));
            }
            pictureBox.Image = mp;
        }
        // Clears fields on form
        private void ClearFields()
        {
            nameBox.Text = "";
            surnameBox.Text = "";
            groupBox.Text = "";
            numberBox.Text = "";
            mobileBox.Text = "";
            pictureBox.Image = new Bitmap(Properties.Resources.Default, new Size(112, 124));
        }
        // Handles event when mouse were clicked on a form
        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            if (listBox.SelectedIndices.Count != 0)
            {
                listBox.SetSelected(listBox.SelectedIndices[0], false);
            }
            ClearFields();
            treeView.SelectedNode = null;
        }
        // Handles event when checkBox state on a form were chenged
        private void treeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (treeCheckBox.Checked)
            {
                LoadTreeView();
            }
            else
            {
                treeView.Nodes.Clear();
            }
        }
        // Handles event when treeView node were selected
        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (listBox.SelectedIndices.Count != 0)
            {
                listBox.SetSelected(listBox.SelectedIndices[0], false);
            }
            var node = treeView.SelectedNode;
            if (node.Nodes.Count != 0)
            {
                return;
            }
            var item = node.Tag;
            ShowContact(item as Contact);
        }
        // Handles event when text in search textBox were changed
        private void finder_TextChanged(object sender, EventArgs e)
        {
            if (finder.Text.Length == 0)
            {
                LoadStorage();
                return;
            }
            listBox.Items.Clear();
            var lst = ContactStorage.FindByNameAndSurname(finder.Text);
            foreach (var item in lst)
            {
                listBox.Items.Add(item);
            }
        }
    }
}
