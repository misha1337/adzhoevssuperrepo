﻿using System.Drawing;

namespace Task06_LINQ_lambda_expressions_UI.Forms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.addButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.treeView = new System.Windows.Forms.TreeView();
            this.treeCheckBox = new System.Windows.Forms.CheckBox();
            this.listBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.surnameBox = new System.Windows.Forms.TextBox();
            this.groupBox = new System.Windows.Forms.TextBox();
            this.numberBox = new System.Windows.Forms.TextBox();
            this.mobileBox = new System.Windows.Forms.TextBox();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.finder = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.Color.Honeydew;
            this.addButton.Location = new System.Drawing.Point(12, 394);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(83, 23);
            this.addButton.TabIndex = 1;
            this.addButton.Text = "Add / Edit contact";
            this.addButton.UseMnemonic = false;
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.BackColor = System.Drawing.Color.Honeydew;
            this.deleteButton.Location = new System.Drawing.Point(141, 394);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(87, 23);
            this.deleteButton.TabIndex = 2;
            this.deleteButton.Text = "Delete selected contact";
            this.deleteButton.UseMnemonic = false;
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // treeView
            // 
            this.treeView.Location = new System.Drawing.Point(255, 200);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(412, 217);
            this.treeView.TabIndex = 4;
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            // 
            // treeCheckBox
            // 
            this.treeCheckBox.AutoSize = true;
            this.treeCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.treeCheckBox.ForeColor = System.Drawing.Color.Black;
            this.treeCheckBox.Location = new System.Drawing.Point(255, 177);
            this.treeCheckBox.Name = "treeCheckBox";
            this.treeCheckBox.Size = new System.Drawing.Size(182, 17);
            this.treeCheckBox.TabIndex = 5;
            this.treeCheckBox.Text = "Show contacts in a tree by group";
            this.treeCheckBox.UseVisualStyleBackColor = false;
            this.treeCheckBox.CheckedChanged += new System.EventHandler(this.treeCheckBox_CheckedChanged);
            // 
            // listBox
            // 
            this.listBox.FormattingEnabled = true;
            this.listBox.Location = new System.Drawing.Point(12, 51);
            this.listBox.Name = "listBox";
            this.listBox.Size = new System.Drawing.Size(216, 329);
            this.listBox.TabIndex = 6;
            this.listBox.SelectedIndexChanged += new System.EventHandler(this.listBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(262, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Contact information: ";
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(255, 31);
            this.nameBox.Name = "nameBox";
            this.nameBox.ReadOnly = true;
            this.nameBox.Size = new System.Drawing.Size(281, 20);
            this.nameBox.TabIndex = 8;
            // 
            // surnameBox
            // 
            this.surnameBox.Location = new System.Drawing.Point(255, 57);
            this.surnameBox.Name = "surnameBox";
            this.surnameBox.ReadOnly = true;
            this.surnameBox.Size = new System.Drawing.Size(281, 20);
            this.surnameBox.TabIndex = 9;
            // 
            // groupBox
            // 
            this.groupBox.Location = new System.Drawing.Point(255, 83);
            this.groupBox.Name = "groupBox";
            this.groupBox.ReadOnly = true;
            this.groupBox.Size = new System.Drawing.Size(281, 20);
            this.groupBox.TabIndex = 10;
            // 
            // numberBox
            // 
            this.numberBox.Location = new System.Drawing.Point(255, 109);
            this.numberBox.Name = "numberBox";
            this.numberBox.ReadOnly = true;
            this.numberBox.Size = new System.Drawing.Size(281, 20);
            this.numberBox.TabIndex = 11;
            // 
            // mobileBox
            // 
            this.mobileBox.Location = new System.Drawing.Point(255, 135);
            this.mobileBox.Name = "mobileBox";
            this.mobileBox.ReadOnly = true;
            this.mobileBox.Size = new System.Drawing.Size(281, 20);
            this.mobileBox.TabIndex = 12;
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.Lime;
            this.pictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox.BackgroundImage")));
            this.pictureBox.Location = new System.Drawing.Point(555, 31);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(112, 124);
            this.pictureBox.TabIndex = 13;
            this.pictureBox.TabStop = false;
            // 
            // finder
            // 
            this.finder.Location = new System.Drawing.Point(12, 13);
            this.finder.Name = "finder";
            this.finder.Size = new System.Drawing.Size(216, 20);
            this.finder.TabIndex = 14;
            this.finder.TextChanged += new System.EventHandler(this.finder_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(680, 433);
            this.Controls.Add(this.finder);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.mobileBox);
            this.Controls.Add(this.numberBox);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.surnameBox);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox);
            this.Controls.Add(this.treeCheckBox);
            this.Controls.Add(this.treeView);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.addButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(696, 472);
            this.MinimumSize = new System.Drawing.Size(696, 472);
            this.Name = "Form1";
            this.RightToLeftLayout = true;
            this.Text = "Contact manager";
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseClick);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.CheckBox treeCheckBox;
        private System.Windows.Forms.ListBox listBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.TextBox surnameBox;
        private System.Windows.Forms.TextBox groupBox;
        private System.Windows.Forms.TextBox numberBox;
        private System.Windows.Forms.TextBox mobileBox;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.TextBox finder;
    }
}

