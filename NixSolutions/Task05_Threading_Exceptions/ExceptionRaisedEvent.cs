﻿using System;

namespace Task05_Threading_Exceptions
{
    // Class which contains exception generation data
    internal class ExceptionCatchedEvent : EventArgs
    {
        public ExceptionCatchedEvent(string threadName, Type exceptionType)
        {
            ExceptionType = exceptionType;
            ThreadName = threadName;
        }
        // Exception type
        public Type ExceptionType { get; set; }
        // Current thread name
        public string ThreadName { get; set; }
        // ToString method overriding
        public override string ToString()
        {
            return string.Format(ThreadName + " " + ExceptionType + Environment.NewLine);
        }
    }
}
