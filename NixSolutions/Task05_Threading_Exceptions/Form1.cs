﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace Task05_Threading_Exceptions
{
    public partial class Form1 : Form
    {
        private bool _isClicked1; // if you pressed the button for the first thread
        private bool _isClicked2; // if you pressed the button for the second thread
        private static readonly object _locker = new object(); // cap for lock
        private readonly Thread _thread1; // first thread
        private readonly Thread _thread2; // second thread
        private event EventHandler<ExceptionCatchedEvent> ExceptionCatchedEvent; // introduce method, which will handle event, when event gives a data
        private readonly Queue<ExceptionCatchedEvent> _exceptions = new Queue<ExceptionCatchedEvent>(); // exceptions queue

        public Form1()
        {
            InitializeComponent();
            _thread1 = new Thread(FirstGenerator) { IsBackground = true, Name = "First thread" };
            _thread2 = new Thread(SecondGenerator) { IsBackground = true, Name = "Second thread" };
            _thread1.Start();
            _thread2.Start();
            ExceptionCatchedEvent += OnExceptionCatched;
        }
        // Form closing event handler
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _thread1.Abort();
            _thread2.Abort();
        }
        // Handles events when exception have been catched
        private void OnExceptionCatched(object sender, ExceptionCatchedEvent e)
        {
            var catchedException = _exceptions.Dequeue();
            Thread.Sleep(100);
            PostMessage(catchedException.ToString());
        }
        // Event pressing the button for stop / start operation of the first thread
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            _isClicked1 = checkBox1.Checked;
        }
        // Event pressing the button for stop / start operation of the second thread
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            _isClicked2 = checkBox2.Checked;
        }
        #region Process of exception generation and posting message to the textbox
        /// <summary>
        /// Exception generation for the first thread
        /// </summary>
        private void FirstGenerator()
        {
            while (true)
            {
                if (!_isClicked1)
                    Generate<OutOfMemoryException>();
            }
        }
        /// <summary>
        /// Exception generation for the second thread
        /// </summary>
        private void SecondGenerator()
        {
            while (true)
            {
                if (!_isClicked2)
                    Generate<FileNotFoundException>();
            }
        }
        /// <summary>
        /// Exception generation process
        /// </summary>
        /// <typeparam name="T">exception type</typeparam>
        private void Generate<T>() where T : Exception, new()
        {
            lock (_locker)
            {
                try
                {
                    throw new T();
                }
                catch (Exception e)
                {
                    var ea = new ExceptionCatchedEvent(Thread.CurrentThread.Name, e.GetType());
                    _exceptions.Enqueue(ea);
                    ExceptionCatchedEvent?.Invoke(this, ea);
                }
            }
        }
        /// <summary>
        /// Thread safety output into the textbox
        /// </summary>
        /// <param name="message"/>For output message(contains exception type and thread name)/param>
        public void PostMessage(string message)
        {
            if (messageLine.InvokeRequired)
            {
                var action = new Action<string>(PostMessage);
                Invoke(action, message);
            }
            else
            {
                messageLine.AppendText(message);
            }
        }
        #endregion
    }
}
