﻿using System;

namespace Task04_Employees
{
    /// <summary>
    /// Employee describer
    /// </summary>
    [Serializable]
    public class Employee
    {
        string _firstName;
        string _lastName;
        int _age;
        string _department;
        string _address;
        string _employeeID;
        /// <summary>
        /// Employeee name
        /// </summary>
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        /// <summary>
        /// Employeee surname
        /// </summary>
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        /// <summary>
        /// Employeee age
        /// </summary>
        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }
        /// <summary>
        /// Employeee department
        /// </summary>
        public string Department
        {
            get { return _department; }
            set { _department = value; }
        }
        /// <summary>
        /// Employeee address
        /// </summary>
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        /// <summary>
        /// Overload for toString method
        /// </summary>
        /// <returns>String with information of an employee</returns>
        public override string ToString()
        {
            return string.Format("Last name: {0}{4}First name: {1}{4}Age: {2}{4}Department: {3}"
                , _lastName, _firstName, _age, _department, Environment.NewLine);
        }
        /// <summary>
        /// Launching employee ID
        /// </summary>
        public void SetID()
        {
            _employeeID = _lastName + _firstName;
        }
    }
}
