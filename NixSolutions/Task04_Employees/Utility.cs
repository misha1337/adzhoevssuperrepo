﻿using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;

namespace Task04_Employees
{
    /// <summary>
    /// Utilitz for handling an employee collection
    /// </summary>
    static class Utility
    {
        /// <summary>
        /// Serializer object to save collection into xml file(the name of root is Employees)
        /// </summary>
        private static XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Employee>), new XmlRootAttribute("Employees"));
        /// <summary>
        /// A private field of employee class object reflection
        /// </summary>
        /// <param name="name">private field name</param>
        /// <param name="emp">class object for reflection</param>
        /// <returns>value of that field(or null)</returns>
        public static object GetField(string name, Employee emp)
        {
            FieldInfo meth = emp.GetType().GetField(name, BindingFlags.Instance | BindingFlags.NonPublic);
            return meth.GetValue(emp);
        }
        /// <summary>
        /// Into xml file collection serialization
        /// </summary>
        /// <param name="filename">filename</param>
        /// <param name="Employees">collection to serialize</param>
        public static void XmlSerialize(string filename, List<Employee> Employees)
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            using (var fs = new FileStream(settings[filename].Value, FileMode.OpenOrCreate))
            {
                xmlSerializer.Serialize(fs, Employees);
            }
        }
        /// <summary>
        /// Deserialize employee collection
        /// </summary>
        /// <param name="filename">filename</param>
        /// <returns>deserialized list</returns>
        public static List<Employee> XmlDeserialize(string filename)
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            List<Employee> employees;
            using (var fs = new FileStream(settings[filename].Value, FileMode.Open))
            {
                employees = (List<Employee>)xmlSerializer.Deserialize(fs);
            }
            foreach (Employee emp in employees)
                emp.SetID();
            return employees;
        }
        /// <summary>
        /// Getting employees list sorted by id and limited by age
        /// </summary>
        /// <param name="employees">list of employee</param>
        /// <param name="startAge">lower bound of age</param>
        /// <param name="endAge">upper bound of age</param>
        /// <returns>employee list</returns>
        public static List<Employee> GetOrderedEmployeesByCriterion(List<Employee> employees, int startAge, int endAge)
        {
            var query = from employee in employees
                         where employee.Age >= startAge && employee.Age <= endAge
                         orderby GetField("_employeeID", employee)
                         select employee;
            List<Employee> result = new List<Employee>(query);
            return result;
        }
    }
}
