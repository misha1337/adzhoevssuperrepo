﻿using System;
using System.Collections.Generic;

namespace Task04_Employees
{
    /// <summary>
    /// Console user interface
    /// </summary>
    static class UserInterface
    {
        /// <summary>
        /// Outputs information about of employees in a list
        /// </summary>
        /// <param name="employees">employee storage</param>
        public static void PrintEmployees(List<Employee> employees)
        {
            int i = 1;
            foreach (Employee employee in employees)
            {
                Console.WriteLine("№" + i + Environment.NewLine + employee
                    + Environment.NewLine + "EmployeeID: " + Utility.GetField("_employeeID", employee)
                    + Environment.NewLine + "Address: " + Utility.GetField("_address", employee) + Environment.NewLine);
                i++;
            }
        }
    }
}
