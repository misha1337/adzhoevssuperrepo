﻿using System.Collections.Generic;

namespace Task04_Employees
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> employees = Utility.XmlDeserialize("Employees");
            UserInterface.PrintEmployees(employees);
            var newEmployees = Utility.GetOrderedEmployeesByCriterion(employees, 25, 35);
            UserInterface.PrintEmployees(newEmployees);
            Utility.XmlSerialize("SortedEmployees", newEmployees);
        }
    }
}
