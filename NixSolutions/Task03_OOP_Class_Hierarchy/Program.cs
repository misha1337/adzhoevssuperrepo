﻿using System.Collections.Generic;

namespace Task03_OOP_Class_Hierarchy
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Creature> creatures = new List<Creature>();
            creatures.Add(new Dog(132, 4));
            creatures.Add(new Dog(253, 4));
            creatures.Add(new Roach(31));
            creatures.Add(new Crucian(624));
            creatures.Add(new Horse(30, 4));
            creatures.Add(new Horse(11, 4));
            int numberOfCreaturesLegs = Utility.GetLegsNumber(creatures);
            System.Console.WriteLine("Number of legs is " + numberOfCreaturesLegs);
            Utility.PrintAllIDs(creatures);
            Utility.GetBreathersIDs(creatures);
        }
    }
}
