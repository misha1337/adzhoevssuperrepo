﻿using System;
using System.Collections.Generic;

namespace Task03_OOP_Class_Hierarchy
{
    /// <summary>
    /// Утилитарный класс с методами получения информации из коллекции живых существ
    /// </summary>
    static class Utility
    {
        public static void PrintAllIDs(List<Creature> creatures)
        {
            Console.WriteLine("All ids: ");
            foreach (Creature creature in creatures)
                Console.WriteLine(creature.GetType() + " | " + creature.CreatureId);
        }
        /// <summary>
        /// Метод получения уникальных идентификаторов каждого существа
        /// </summary>
        /// <param name="creatures">коллекция существ</param>
        public static void GetBreathersIDs(List<Creature> creatures)
        {
            Console.WriteLine("Creatures who can breath under the water: ");
            foreach(Creature creature in creatures)
            {
                if (creature is IUnderWaterBreather)
                {
                    Console.WriteLine(creature.CreatureId);
                }
            }
        }
        /// <summary>
        /// Метод получения общего количества ног у существ в коллекции
        /// </summary>
        /// <param name="creatures">коллекция для расчета</param>
        /// <returns>общее количество ног</returns>
        public static int GetLegsNumber(List<Creature> creatures)
        {
            int legsNumber = 0;
            foreach (Creature creature in creatures)
            {
                if (creature is Animal)
                {
                    legsNumber += (creature as Animal).NumberOfLegs;
                }              
            }
            return legsNumber;
        }
    }
}
