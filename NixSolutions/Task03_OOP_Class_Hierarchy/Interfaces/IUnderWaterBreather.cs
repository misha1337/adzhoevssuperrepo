﻿namespace Task03_OOP_Class_Hierarchy
{
    /// <summary>
    /// Интерфейс с сигнатурой метода дышания под водой
    /// </summary>
    interface IUnderWaterBreather
    {
        /// <summary>
        /// Сигнатура метода дышания под водой
        /// </summary>
        void BreathUnderWater();
    }
}
