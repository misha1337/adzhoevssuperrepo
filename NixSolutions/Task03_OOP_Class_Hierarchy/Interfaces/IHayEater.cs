﻿namespace Task03_OOP_Class_Hierarchy
{
    /// <summary>
    /// Интерфейс с сигнатурой метода поедания сена.
    /// </summary>
    interface IHayEater
    {
        /// <summary>
        /// Сигнатура метода поедания сена.
        /// </summary>
        void EatHay();
    }
}
