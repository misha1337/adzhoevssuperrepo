﻿using System;

namespace Task03_OOP_Class_Hierarchy
{
    /// <summary>
    /// Класс описывающий сущность "Карась", который является рыбой и может есть сено
    /// </summary>
    class Crucian : Fish, IHayEater
    {
        public Crucian(int creatureId) : base(creatureId) { }
        /// <summary>
        /// Метод поедания сена
        /// </summary>
        public void EatHay()
        {
            Console.WriteLine("I'm a crucian and I can breath under the water.");
        }
    }
}
