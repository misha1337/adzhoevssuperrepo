﻿using System;

namespace Task03_OOP_Class_Hierarchy
{
    /// <summary>
    /// Класс описывабщий сущность "Лошадь", которая является животным и может есть сено.
    /// </summary>
    class Horse : Animal, IHayEater
    {
        public Horse(int creatureId, int numberOfLegs) : base(creatureId, numberOfLegs) { }
        /// <summary>
        /// Метод поедания сена.
        /// </summary>
        public void EatHay()
        {
            Console.WriteLine("I'm a horse and I can eat hay.");
        }
    }
}
