﻿using System;

namespace Task03_OOP_Class_Hierarchy
{
    /// <summary>
    /// Класс описывающий сущность "Собака", которая является животным и может дышать под водой
    /// </summary>
    class Dog : Animal, IUnderWaterBreather
    {
        public Dog(int creatureId, int numberOfLegs) : base (creatureId, numberOfLegs) { }
        /// <summary>
        /// Метод дышания под водой
        /// </summary>
        public void BreathUnderWater()
        {
            Console.WriteLine("I'm a dog and I can breath under the water.");
        }
    }
}
