﻿namespace Task03_OOP_Class_Hierarchy
{
    /// <summary>
    /// Класс описывающий сущность "Плотва", которая является рыбой
    /// </summary>
    class Roach : Fish
    {
        public Roach(int creatureId) : base(creatureId) { }
    }
}
