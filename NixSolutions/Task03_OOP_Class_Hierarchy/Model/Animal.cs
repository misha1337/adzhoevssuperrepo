﻿namespace Task03_OOP_Class_Hierarchy
{
    /// <summary>
    /// Класс описысающий сущность  "Животное"
    /// </summary>
    class Animal : Creature
    {
        /// <summary>
        /// Количество ног у животного
        /// </summary>
        public int NumberOfLegs { get; set; }
        protected Animal(int creatureId, int numberOfLegs)
        {
            CreatureId = creatureId;
            NumberOfLegs = numberOfLegs;
        }
    }
}
