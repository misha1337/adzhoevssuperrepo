﻿using System;

namespace Task03_OOP_Class_Hierarchy
{
    /// <summary>
    /// Класс описыващий сущность рыба, которая является живым существом и может дышать под водой
    /// </summary>
    class Fish : Creature, IUnderWaterBreather
    {
        protected Fish(int creatureId)
        {
            CreatureId = creatureId;
        }
        /// <summary>
        /// Метод дышания под водой
        /// </summary>
        public void BreathUnderWater()
        {
            string type = GetType().ToString();
            Console.WriteLine("I'm a " + type.Substring(type.IndexOf('.')+1) + " and I can breath under the water.");
        }
    }
}
