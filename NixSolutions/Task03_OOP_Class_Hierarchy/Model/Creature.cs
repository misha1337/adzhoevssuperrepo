﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task03_OOP_Class_Hierarchy
{
    /// <summary>
    /// Класс описывающий абстрактную сущность "Живое существо"
    /// </summary>
    abstract class Creature
    {
        /// <summary>
        /// Уникальный идентификатор существа
        /// </summary>
        public int CreatureId { get; set; }
    }
}
