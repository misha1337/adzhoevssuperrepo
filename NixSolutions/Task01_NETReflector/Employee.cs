﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task01_NETReflector
{
    class Employee
    {
        public string Name;
        private int _age;
        public Employee()
        {

        }

        public Employee(string name)
        {
            Name = name;
        }

        public Employee(string name, int age)
        {
            Name = name;
            _age = age;
        }

        public void Work()
        {
            //work..
        }

        private void GoHome()
        {
            //going home..
        }
    }
}
