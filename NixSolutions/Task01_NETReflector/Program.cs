﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task01_NETReflector
{
    class Program
    {
        static void Main(string[] args)
        {
            Factory factory = new Factory();
            factory.emps.Add(new Employee());
            factory.emps.Add(new Employee("Henry"));
            factory.emps.Add(new Employee("Henry", 33));
            Dog charlie = new Dog();
            charlie.Name = "charlie";
            charlie.Weight = 37;
        }
    }
}
