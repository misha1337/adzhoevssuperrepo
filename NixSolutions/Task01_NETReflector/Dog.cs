﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task01_NETReflector
{
    struct Dog
    {
        public int Weight;
        public string Name;
        private int _height;

        public void MakeNoise()
        {
            //making noise..
        }

        private void Sleep()
        {
            //sleeping..
        }
    }
}
